# The extrusion process  


Finally, it has been time to analyze the extrusion process, under the main source [^1].  
This section is organized as an introduction to the process of extrusion, through the help of  scientific sources, and an analysis on the specifics of <span style="color:blue">PLA</span>, the material that has been treated for the fabrication of the recycled filament.
Successively, it will follow a section with a tutorial on how to use the extruder and, finally, there will be an analysis on the diameter of the filament considering the physical forces which acts on the extruded <span style="color:blue">PLA</span>.


## The main scientific basis of extrusion


The extrusion process involves a physical change in the polymer chain.  
Let's recall the definition of polymers: [^1]  
<span style="color:blue">Definition</span>: Polymers are long-chain molecules with one or more repeat units called monomers.  
The number of repeat units in a polymer, and thus the length of the polymer chain, can be varied during manufacture of the resin.  
The molecular weight of a polymer is a way of indicating chain length.  

To understand why the process of reorganization of these chains is possible, it is necessary to pay attention to the following figure:

![viscosity](../images/final_project/extrusion/viscosity.png)  
*Illustration of the log shear rate \( \dot \gamma  \) (x-axis) in function of the log of the kinematic viscosity(  \( \eta=\frac{\mu}{\rho} \) ).  \( \mu \) is called dynamic viscosity or also shear viscosity.*

The y-axis shows the **log of the viscosity**, while on the x-axis we find the **log shear rate**.
The viscosity, with the increase of temperature, decreases and it implies to a major malleability of the thermoplastic, with the passage toward the fluid regime.    
<span style="color:blue">Definition</span>: **The shear rate** is the rate at which fluid layers move past each other, supposing that the fluid could be divided in horizontal layers.  
In the reference [^10], it is defined as:  

\[ \sigma= \mu \frac{v}{d} = \mu \dot \gamma = \mu \frac{\partial v}{\partial x} \]


![](../images/final_project/extrusion/couette_plan.png)  
*Image which shows the definition of viscosity and shear rate in a simple case. Credits to reference [^10].*

**Shear rate** can also be defined as the rate at which a progressive shearing deformation is applied to some material.  
Therefore, one can define **the kinematic viscosity** \( \eta  [Pa \cdot s] \) as the coefficient which relates **the shear stress**(\( \tau [Pa] \)) and **the shear rate**(\( \dot \gamma [s^{-1}]  \)) which depends on the material and on the system:

\[\tau= \eta \dot \gamma\]

The shear rate varies considerably with processing method, for the extrusion that interests us the value is between 100 \( s^{-1}\)  and 1000 \( s^{-1}\).
The **shear stress** is the force between the layers, and is relied to **the stress** \( \sigma \) via a constitutive relation.
The article [^2] suggests to consider PLA as non-Newtonian fluid as a better approximation.
Nevertheless, for our didactic purpose, we have considered it as Newtonian.  
<span style="color:blue">Definition</span>: Newtonian fluid means that the relation between the shear rate and the shear stress is linear.


### Application on PLA


The curve of PLA, which shows how the shear rate varies with temperature, has been applied as a main reference for my experiment.


![curve_of_pla2](../images/final_project/extrusion/pla_curve2.png)  
*Figure which shows how the shear rate(x-axis) varies with the shear viscosity for PLA. The legend shows that, depending on the temperature of extrusion, a different law can be obtained [^7].*

This figure justifies then the fact that the form and the dimension of the new filament will strongly depend on the temperature of extrusion.

<span style="color:red">Important notice</span>: During the experiment for the creation of the filament, the machine allows to set a temperature. This temperature will then set the curve.

<span style="color:green">In my experiment </span> the temperature will be then considered as constant during the process of extrusion.  
<span style="color:orange">In general</span>, it could be also possible to control the shear rate and vary the viscosity in function of the temperature.
This is why a little section is dedicated to the model of the viscosity in function of T.
#### Viscosity in function of temperature(T) for PLA

There are different models that can be built about \( \eta \) in function of T.

A law which describes this property of plastic is given by the Arrhenius equations, which link viscosity with the temperature[^1]:

\[ \eta=A exp(\frac{E_a}{RT})\]

where A is constant, \( E_a \) is the activation energy (\(cal \hspace{2mm} mol^{-1}\)), R is universal gas constant (\(J \hspace{3mm} K^{-1} mol^{-1} \) ) and T is the temperature in Kelvin.



The article [^4] has measured the following value, including the constant A (\( R = 0.001987 \hspace{3mm} kcal K^{-1} mol{-1}  \))[^4]:

\[  ln(A) = 102.3 , \hspace{6mm} E = 0.06865 \hspace{2mm} kcal \hspace{2mm}mol^{-1}              \]


Another model is proposed by **Landel-Ferry**[^4] and is more general[^1]:

\[\eta=\eta_r   exp[-\frac{C_1(T-T_r)}{C_2+(T-T_r)}],\]

where \( \eta_r  \) is the viscosity at a reference temperature \( T_r  \), \(  C_1 \) and  \(  C_2 \) are material constants and are used for wider temperature ranges.

For PLA, the article gives the values [^4]:

\[ C_1= 17.14 \hspace{2mm} C_2=85.71  \]

This section has been conceived to give some insight into the model and to inspire the curious reader.
## The process of extrusion

<span style="color:blue">Definition</span>:**Extrusion** is a polymer conversion operation in which a solid thermoplastic material is melted or softened, forced through an orifice (die) of the desired cross section, and cooled.
An example of extruder is proposed in the next figure, although the extruder at the FabLab is slightly different.

![extruder](../images/final_project/extrusion/extruder.png)  
*Illustration of a model of extruder in order to describe the mechanism. Credits to [^1].*    

More detailed information on the technology behind the extruder can be found in appendix.

These machines allow to adapt the temperature of extrusion to the material.  
<span style="color:red">Safety advice</span>:The uniformity of the material here is a strong requirement in order not to burn plastic.  
The process is delicate because, going over the melting temperature, could burn plastic and release toxic residuals in the air.  



The Fablab already hold one of these machines, in particular the article is sold on [the following site](https://www.bbfil.fr/extrudeuse-noztek-pro-c2x22851481).  

![machine extruder](../images/final_project/extrusion/extruder_machine.jpeg)  
*Image of the Noztek-pro, the extruder provided by the FabLab ULB.*  

The main characteristics of the machine can be found on the following image:  

![machine extruder characteristics](../images/final_project/extrusion/extruder_machine_characteristics.jpeg)  
*Main characteristics of the Noztek-pro. Other important information can find on the official site.*  

The manual for the extruder, consulted before using it, can be found [at the official page](http://www.noztek.com/wp-content/uploads/2019/05/Noztek-Pro-user-manual-final.pdf).


In order to use the machine it is needed to use a shredder to break plastic in little flakes.
Nevertheless, the FabLab did not hold a shredder for plastic.
Therefore, with the aim of shredding, I decided to create a partnership between **The plastic factory** in Brussels and the **Fablab ULB**.  
Every information about the reality of precious plastic can be found at [their official site](https://www.plasticfactory.be/).  
The description of the process to obtain the shredded pieces of PLA has been described in the precedent section of the project.




### The first tentative to create the filament
The possibility to test the creation of the filament from the fourth week has been a crucial point.  
As a matter of facts, this week has allowed me to improve my knowledge on the extruder machine of the FabLab.
The extrusion process I am going to describe differs from the extrusion of plastic in the 3D printer.
For example, for the material that will be recycled, <span style="color:blue">**PLA**</span>, the extrusion temperature in the 3D printer is 220°.  
Meanwhile, in the extruder machine, the producer advises a range of <span style="color:blue">165°-175°</span> where the higher temperature corresponds to a thinner filament.
As a matter of facts, the machine allows to set the temperature of the material and the speed of the flow in rounds per minutes.
A vent, which is installed at the exit of the extruded filament, did not work properly for the experiment.  
It has generated some issues in the creation of the filament.  

#### Errors commited

During the first approach to the machine several errors have been committed and more attention should have been payed to the **user manual**[^5].

##### <span style="color:red">Errors on the temperature of extrusion</span>
The **first error** has been the temperature, in particular I put 220° which was too high for making a filament with PLA.  
The right temperature for PLA, applied during the try in the fifth week, is between 165° and 175°.  
Another **important advice**, which has been given by the Plastic factory members, is that plastics should not be mixed in order to avoid to burn part of them.  
Let's remind that the burning of plastic releases toxic compounds in the air and it is really dangerous especially in closed environments.  
**improvement that has been applied for the following weeks**: Apply the right temperature.  




##### <span style="color:red">Errors on the dimension of the plastic flakes(pellet)</span>
The dimension of the little fragments of plastic to inject into the extruder is crucial.  
As a matter of facts, serious issues have been experimented in the first try for the dimension of the fragments which blocked the mechanism.    
![first_configuration](../images/final_project/extrusion/first_configuration.JPEG)  
*First configuration of try of the machine.*    
As a matter of facts, the central part of the extruder(**the hopper**), which transports plastic following its rotation, has been blocked by little structures of plastic which were not perfectly suitable in dimensions.  
The machine does not allow to revert the sens of rotation of the hopper.  
In particular, with the help of Axel(a FabLAb mentor) the machine has been opened and we found an agglomeration around the hopper.  

![agglomeration](../images/final_project/extrusion/agglomeration.JPEG)  
*Agglomeration of plastic that blocked the hopper.*  
In order to solve this problem three ways are proposed:

* Activate the machine at 240° for 20 minutes at the speed of 20 in order to free the channel from plastic.  We have tried this method but it does not have worked.
* Use a blow torch to heat the barrel just past the hopper, which will usually release any solidified plastic and free the screw.
* Disassemble the machine.


For this time, we have decided to follow the third way in order to study the internal details of the machine.  
<span style="color:yellow">Acknowledgment</span>:I thank Axel for the help with the machine in this delicate process.


#### The description of each piece

The machine has been decomposed and here I report the main features:

* The following structure is the contener for the fragments of plastic.  
![contener](../images/final_project/extrusion/contener.JPEG)  
*Photo of the contener of the machine.*  
* There is a fan, which must be activated in the menu under the attribute "fan on"
![vent](../images/final_project/extrusion/vent.jpeg)  
*Photo of the vent of the machine.*
* The filament's diameter depends on the exiting structure. It can be changed in order to obtain different diameters.
![adaptator](../images/final_project/extrusion/adaptator.JPEG)  
*Image of the nozzle of the extruder.*  
* The internal mechanism with the motor and an Arduino in order to capt Temperature and program the motor.
![internalstructre](../images/final_project/extrusion/internal_structure2.JPEG)  
*Photo of the internal structure of the extruder. Notice the Arduino which is relied to a sensor of temperature.*

The structure has been freed from the plastic pieces as it is briefly shown in this video:  

<video controls muted>
<source src="../../images/final_project/extrusion/liberation.mp4" type="video/mp4">
</video>




### The vent
The vent is a fundamental part of the machine because it allows to cool the flow of plastic after the extrusion avoiding that external forces could modify the diameter.  
In particular, the following schema proposes the different forces that apply on the filament.


![forces](../images/final_project/extrusion/forces.png)  
*Simple scheme of the forces that take place in the process. The code of colors allows to identify the forces: <span style="color:blue">gravity force</span>, <span style="color:yellow">the pressure of the machine</span>, <span style="color:white">the tension due to the person/coil that turn</span>, <span style="color:green">the forces given thanks to the fan</span>.*

In this figure we find:

* The gravity force \( F_g\) which is defined as:  
 \( \textbf{F}_g=\textbf{g} \cdot m \)  
  where m is the center of mass which depends on the length of the wire, the diameter of each part of the wire(if non-homogeneous) and the speed of the extruder.
 * The second forces is exercised by the the tension of the filament.
  As it will be described, it could be both the human hands and a mechanism which wraps the wire around the coil.  
 In particular, depending on the mechanisms we described before, a force of tension \( F_t \), which is function of the viscosity of the filament \( \eta \) and the wrapping speed due to the motor.
 In case of constant viscosity, because the extrusion temperature \( T_e \) is constant, the force could be considered constant.
 * The pressure force, which is applied on the material from the extruder, can be expressed via the constraint tensor \( \tau_{ij} \), which includes the pressure on the fluid \( -p \delta_{ij} \) and the stress applied with a component which depends on \( \epsilon \) .  
 The pressure can be achieved via the insertion of several quantity of material to extrude in order to assure a continuity of the flow.
 * Last but not least, the vent also exercises a vertical force.
 In most of my experiences the vent was not functioning, hence, some imperfection on the filament can be noticed.
 Nevertheless, the quality has been enough to print with it.  
 A new vent has been installed and it has a tension of 12V and a current of 0.13A, confronted to 24V and 0.17A of the precedent vent.
 It implies that, for the moment, it does not still provide excellent results.



As described in the documentation phase, when pulling a filament, a poisson coefficient must be introduced to rely the elongation on the principal axis(horizontal to the wire in this case) with a diminution of the vertical dimension.
This is the factor that determines the thinner filament if the filament is pulled too much.
In order to avoid to pull the filament by hand, another method has been introduced.

### The second method to manage the exiting filament

Another method to wrap the wire around the coil is the use of a structure, here in video.

<video controls muted>
<source src="../../images/final_project/extrusion/motor.mp4" type="video/mp4">
</video>

It is composed by:

* A motor, which is relied to a tension generator. The modulation of the voltage allows to modify the speed of the motor.
* A tension generator, which however only allows to work at constant velocity of rotation.


### Test on the diameter of the filament

I have decided to test the two methods at different temperatures in order to give the best method to make a new filament.
This part of the project has involved the use of measurements with a digital caliper.  
![caliper](../images/final_project/extrusion/calliper.JPEG)  
*Image of the caliper, used to measure the diameter of the filament.*  
It has been decided to test different temperatures maintaining the speed constant.
Two kind of methods, in order to test the efficiency and the diameter, have been tried.
For each one a value of \( T_e \) has involved a range between 150 and 175, which means the maximal temperature advised by the producer.

It has been verified that the best range of temperature was from \( 165 °-175°\) with a preference for the 165° in order to have a thicker filament.
For what it regards the method to wrap the filament, it is better to turn by hand the coil in order to adapt to the different velocities of the flow.
The other method could be improved with the insertion of sensors which capt when the filament is under a certain height as it is shown [in the following video](https://www.youtube.com/watch?v=vqWwUx8l_Io).

Here I show a video which resumes how I proceeded for the creation of the filament thorough the motor.



<video controls muted>
<source src="../../images/final_project/extrusion/motor_filament.mp4" type="video/mp4">
</video>

An equivalent proceeding has been realized simply turning the coil by the hand.

The final result has been the obtainment of a filament from 1.40mm of diameter up to 1.70mm.

For every experiment, the velocity has been left to \(\omega= 10 \frac{round}{minutes} \)(the minimal one) in order to reduce the  inhomogenity the filament.

### Filament thickness deviation (inconsistent diameter), a common problem in the 3D printing

Another problem that I have found with my filament is the deviation of the thickness, hence inconsistent diameter.
The source has been the merchandiser 3devo[^10].
It explains that a very common problem during the extrusion of 3D-printing filament is the variation of the filament thickness or diameter, even at the industrial level.

It identifies the main factors that can cause this issue, they are just cited here but treated in detail in the reference[^10]:

* External factors, strongly linked to temperature.
* Temperature settings of the extruder.
* Extruder RPM.  
The extruder RPM stands for the rounds per minute that the extruder’s screw is turning, and therefore defines the output rate of the material.
By reducing the RPM, the fluctuations will also be reduced.  
This is why in the extrusion process, I have operated at the minimal round per minutes of 10.
* Polymer degradation, which is the change in properties of a polymer under the influence of environmental factors such as heat, oxygen, light or chemicals.
A possible chemical reaction is polymer cross-linking, which makes a polymer more rigid, stronger, or thermally stable showing the behavior of a thermoset.
This can reduce the flow ability of the material through the extruder and the produced filament might be impossible to 3D-print because the melting temperature might have become much higher, or the filament might not even melt at all.
In the case of extrusion, the polymer will form cross-links under the influence of pressure and heat.

![cross_link](../images/final_project/printing_process/Figure-7-Filament-thickness-deviation.jpg)
Figure which explains the phenomenon of cross-link. Credits to [^10]  

* Material contamination, due to the impurities of the input material.
* Too much cooling from the fans, which makes the filament too rigid.
* Winder mechanism, which means that there could be a filament stretching effect caused by high tension of the filament.
* Dirt in the extruder.
* Holes or bubbles in filament.
* An inconsistent refilling of the pellet, which could cause inhomogeneity of the material.

## Appendix


### The functioning of the extruder

The machine works as it follows: starting from resins particles, a filament can be obtained.  
The resins particles are fed from the hopper to the extruder.  
The resins are then packed thanks to the rotating screw, and so the need for a motor.    
As a matter of fact, once the plastic particles reach the feed zone, they are compacted into a solid bed.  
Pressure, proportional to temperature, is then increased.  
The bed is conveyed in the transition zone and melted thanks to a **viscous drag force**.  
Moreover, speaking of forces, the gravity and the rotation of the screw are really important to consider.  

An important component of the extruder is **the barrel**.
It is a metal cylinder that surrounds the screw.
One end fastens to the feed throat and the opposite end connects directly to the die adapter.
The extruder barrels must withstand pressures up to 70 \( MPa  \).  
The die can eventually be adapted in order to obtain the desired section for the filament.  
There are heating circuits that allow the raise of temperature and, in order to make a uniform temperature of the barrel, jacketed barrels are installed granting cool efficiently.  

Finally, the extruder's screw are distinguished by their outside diameter (D) and the L/D factor, which is given by:

\[   \frac{L}{D}=\frac{L_{flight}}{D}, \]

where L flight is the length of the flighted section.
I also add here am image where some nomenclature is shown.
![nomenclature](../images/final_project/extrusion/nomenclature.png)  
*Image of the nomenclature for a model of extruder. Credits to [^1].*




## References

[^1]:"Harper, Charles A. Modern plastics handbook. McGraw-Hill Education, 2000.", Chapter 5
[^9]:Course of the ULB university on Continuum mechanics, professor  Fabian Brau, 2021.
[^2]:Fang, Q. and Hanna, M.A., 1999. Rheological properties of amorphous and semicrystalline polylactic acid polymers. Industrial Crops and Products, 10(1), pp.47-53.
[^3]:Felfel, R.M., Hossain, K.M.Z., Parsons, A.J., Rudd, C.D. and Ahmed, I., 2015. Accelerated in vitro degradation properties of polylactic acid/phosphate glass fibre composites. Journal of Materials Science, 50(11), pp.3942-3955.
[^4]:Srithep, Y., Nealey, P. and Turng, L.S., 2013. Effects of annealing time and temperature on the crystallinity and heat resistance behavior of injection‐molded poly (lactic acid). Polymer Engineering & Science, 53(3), pp.580-588.
[^5]:[User manual of noztek pro](http://www.noztek.com/wp-content/uploads/2019/05/Noztek-Pro-user-manual-final.pdf)
[^7]:Cobos, C.M., Garzón, L., Martinez, J.L., Fenollar, O. and Ferrandiz, S., 2019. Study of thermal and rheological properties of PLA loaded with carbon and halloysite nanotubes for additive manufacturing. Rapid Prototyping Journal.
[^8]:Hirst, Linda S. Fundamentals of soft matter science. CRC press, 2019.
[^10]:[3devo site, causes for the inconsistent diameter](https://support.3devo.com/filament-thickness-deviation-inconsistent-diameter/)
