# The shredding of the material

At this moment of the project, I had understood what is plastic and which was my final objective.
Nevertheless, before printing with a new filament two steps were still needed:

* The shredding of plastic to obtain little pieces of plastic to use in the mechanism of extrusion.
* The creation of the filament, which will be the topic of the next page.

This section of the project will be dedicated on the process of shredding.  
The process consists in breaking plastic applying a shear on the material, which breaks at a certain point the polymer chain.  
The process of **selection of the material to break** is really delicate and can influence the result.    
This is why I have decided to create a little tutorial on what kind of PLA piece of plastic is useful to break.
Before entering in this detail, I will provide a description of my experience in the **plastic factory** and I will also describe the functioning of the shredder.
As explained, every notion for the curious reader will be in appendix and only the essential characteristics will be included in the text.  

I have received from the FabLab managers of Brussels four boxes like that:
![boxes](../images/final_project/shredding/material_torecycle.JPEG)  
*Box with wastes of PLA from the FabLab.*

I have also capted some information from the FabLab managers about the amount of wastes in which they communicate that:  

- Approximately, it is possible to consider two IKEA boxes each 4 months for a course of 30 students.  
It must be also considered that a part of the student from the precedent course of the FabLab did not apply the recycling of materials for several reasons.  
The main one is that their activity was only once a week.  
For what it regards the potentiality of the projects, an estimation is that it could be possible to recover the double of the material if the people, working on the 3D printer, were aware of the perspective of their material.


Nevertheless, the FabLab itself did not have any shredder, which is the machine used to break plastic.
Therefore, I made reference to the Plastic Factory of Brussels.



## My first experience in the plastic factory

An important step, when looking for help, is to find a mentor.
A mentor is a person who is passionate with his job and would like to transmit something to the others.
In my case, I found in Mathilde Rulens the mentor for the plastic shredding.  
Without hesitation, I have contacted the official mail from the site and took a meeting with her.
The plastic factory is a natural derivation of the precious plastic project.  
In particular, the founding members have started from **the precious plastic manual** that can be found [at this page](https://usermanual.wiki/Pdf/Manual.308585651.pdf).
Following the tutorials on the precious plastic site, they have arrived to build extraordinary efficient machines.  
**Important notice**: each photo of the plastic factory is here published under the accordance of its manager Mathilde Rulens.  

As a first approach, I made a tour in the laboratory and here I show some shots I took of their machines that are used to work with plastic:

* The machine which applies pressure on plastic     
![pressure2](../images/final_project/shredding/presse_factory.png)  
*Press machine in order to make plate of recycled plastic. Courtesy of the plastic factory.*
* An oven for plastic and an extruder is also present.  
![othermachines](../images/final_project/shredding/fourandextruder.png)  
*Oven and extruder in the plastic factory. Courtesy of the plastic factory.*
* They also have one **shredder**, which is the main interest of my visit.
The shredder has been built by his father under the precious plastic tutorial.



### The shredder

The shredder is the machine which gives back, starting from pieces of plastic, a compound of plastic pellets.  
The pellets have then been used for the extrusion process.  

![shredder](../images/final_project/shredding/shredder.png)  
*Model of shredder proposed by precious plastic. The photo has been taken from the reference [^4]*  
This machine can be used **following important recommendations**:

* Do not touch the part of the motor.
* Pay attention to the part where the cutters are present.
* Verify that the shredder is switched off before working on the machine.

The plastic from 3D printing should not be washed because it is usually clean enough to be recycled immediately.

The manager, during the first meeting, proposed to shredder already a set of PLA which had been given to her from another student.  
![plafromplasticfactory](../images/final_project/shredding/pla_fromplasticfactory.JPEG)    
*PLA wastes which have been shredded as a first try. Courtesy of the plastic factory.*  
I upload here a short video in order to understand how to use the shredder.  

<video controls muted>
<source src="../../images/final_project/shredding/shredderhowitworks.mp4" type="video/mp4">
</video>  
*Courtesy of the plastic factory.*

The blades are the most important structure and are here presented:  
![blades](../images/final_project/shredding/shredder_dents.png)  
*Image of the blades of the shredder in the plastic factory center. Courtesy of plastic factory.*

They apply the shear on the pieces of plastic in order to break them.  





With reference to the **shredder machine**, it could be considered as an essential element of the recycling process for the filament of the 3D printer.  

A good reference about the shredder is the " Aryani, Nuri, Dede Buchori, and Albertus Budi Setiawan. "Design of a Plastic Shredder Machine." IPTEK Journal of Proceedings Series 3 (2019): 35-39" [^1].  
The principal step is crush plastic, after an attentive selection, until it becomes small flakes.
The shredder machine has an easy conception:

* Blades or cutter, in order to cut the plastic with blades made of metal in order to be resistant and long-lasting.
* A spur gear, a sort of cylinder around with the blades can turn.
* An electric motor in order to activate the mechanism.
* The body of the machine, included the container where the plastic should be stored.




The two main forces that must apply for the constructions are the **shear** and **tension** forces.  
<span style="color:blue">Definition</span> :**The shear forces** are defined as a stress which is not applied uniformly on the surface and tends then then to modify the object, in particular breaking it.
In solid mechanic, the shear is necessary for cutting the material.

Plastic, when solid, can withstand up to a certain pressure before breaking.  
<span style="color:blue">Definition</span> :**Tensile strength** is defined as a stress, which is measured as force per unit area and varies between the materials.  
The focus on the shredder in appendix is really interesting to understand that each material requires a difference force to break.
If this force can not be obtained with the shredder at disposition, some improvements should be applied.

The shredder tutorial is a really helpful element for a future project with particular reference on how to build [the plastic shredder](https://community.preciousplastic.com/academy/build/shredder).

Now arises the question on <span style="color:red">**how to build a shredder?**</span>  
These are the technical information about the shredder to know before building it.  
![shreddertobuild](../images/final_project/shredding/shredderparameters.png)  
*This image shows the technical informations about the shredder proposed by the precious plastic site. Taken from the reference [^4]*    
The git, where the information on the shredder from the precious plastic can be accessed, is at [this link](https://github.com/hakkens/precious-plastic-kit).

Nevertheless, in the FabLab I was not able to build it from zero, due to the lack of materials.  
This is the reason why I decided to ask to the plastic factory.


## Tutorial on the shredding phase

The main section of this page has been dedicated to a sort of step by step tutorial, which should be a main reference for the FabLab.

Here, a single material, <span style="color:red">PLA</span>  , has been used for the recycling process.
Nevertheless, the form of the object and its density modify the efficiency of the shredding.   


In this first photo it is shown the first result of the shredding process:  

![shredded_material](../images/final_project/shredding/first_pellet.JPEG)  
*Illustration of the first result of the shredding. The metal filter has been used as a reference on the size of the pellets(little pieces of plastic).*

 The pieces of plastic were not enough shredded, as it can be observed with this illustration, in which they were about $1cm$.

![shredded_material_measure](../images/final_project/shredding/measure_of_piece.JPEG)
*Illustration of the size of the pellets in the first try.*
This kind of size has blocked the use of the extrusion machine, as it will be shown in the next page.
Therefore, after a long process to free the machine, I have mixed the plastic a second time in the mixer in order to reduce their size.

A common mixer has been used in order to test the extrusion machine:  
![shacker](../images/final_project/shredding/shaker.JPEG)  
*Illustration of the mixer that has been used.*

The following result has been obtained after the mixing:  
![aftermixing6](../images/final_project/shredding/after_mixing.JPEG)
*Final pellets of plastic that have been used for the extruder.*


This after-mixing material is useful for the extrusion of the new filament.
Even though the final result of shredding has been achieved, there has been an issue from the start in the choice of the wastes to shredder and in the conception of the shredder.
As we will show, in the absence of the perfect shredder, an important point is the density of the printed material.  
If a certain material is too tough or not so elastic, for example the kind of structures in figure, it is better to let them apart for the first phase of recycling.
This has been the <span style="color:blue">**spiral philosophy**</span> that I have applied.

![scarti](../images/final_project/shredding/kind_of_tough_plastic.JPEG)  
*Structures that can difficultly be shredded.Courtesy of the plastic factory.*  



On the contrary, the materials in the next figure are good materials to shredder because they are not as tough and dense as the others.

![buono](../images/final_project/shredding/good_materials.JPEG)  
*Structures that can easily be shredded, the shredding of a filament(for example the red one) deserves some attention because it can pass the shredder without being cut.Courtesy of the plastic factory.*  
From two IKEA boxes, I could select half of them as this kind of material.
The following result has been obtained after the first shredding:  
![materials_obtained](../images/final_project/shredding/material_obtained.JPEG)  
*Final result of the shredding process with the material of the FabLab. This material has been enough to extrude for about three weeks.  Courtesy of the plastic factory.*  

I also tried to pass it again in the shredder in order to reduce the employement of the mixer but it is **inadvisable** because, as I show in figure, the shredder tends to block:  

![blocked](../images/final_project/shredding/shredder_blocked.PNG)  
*Image in which it is shown how the little pieces of plastics tend to block the shredder. Moreover, the heat generated by the motor could create some issues when in contact with plastic.*  

I have decided to make sure that, back at the FabLab, the size of the plastic pieces did not exceed \( \sim mm\) in order to test the limits of the extruder.

Therefore, during the experimental proceedings, the following filter has been applied:  

![filter](../images/final_project/shredding/filter.JPEG)  
*Image of the filter with a size of the holes on the order of millimeters.*  

This filter allows to avoid the insertion of bigger structure of plastic in the extruder:  
![too_big](../images/final_project/shredding/toobig_plastic.JPEG)  
*Examples of fragments that do not pass the test of the filter. They could eventually be used after a second shredding.*

I have weighted the material that I obtained as \(116 g\), which can however guarantee hours of tries during the extrusion.  
It must be underlined that a strict selection has been applied and, therefore, the quantitative could have been almost doubled.  


## Tutorial on the selection phase

We focus in this part what kind of PLA waste has result easier to recycle, being clear that the other kind of structures can be recycled but required more attention for the extrusion process.
I have prepared a set of slides, where I show some examples of the selection of the material.  
I thought at the start that the process of plastic recycling required scientific-advanced notions and, even though it is true, I also realized that the experience is a crucial element.
It helps to understand if the material that we are using is right or not just managing it.  

<span style="color:red">**Not to miss**</span>:Here I add the [link](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-experiments/students/simone.vitale/-/blob/master/docs/project/tutorial_recycling_simone_vitale.pdf)
to the tutorial on the selection of plastic.





## Appendix



### The shredder[^1]

The interest of the article is the description of the kind of waste plastic which could be used for shredding.  
Some examples of plastics to recycle are :

* LDPE(Low Density Propylene) defined by a density range of \( 917-930 kg/m^3 \),
* HDPE(High Density Propylene)(\( 930-970 kg/m^3 \)) that is usually applied in industry and in 3D machine filament.  
* PP(prolypropilene), with a density of \( 895-920 kg/m^3 \),
* polysterene (PS) with a density of \( 1050 kg/m^3 \).  

The design of plastic shredder machine can produce small flake with the dimension of about 10 mm long and 1 mm wide for these materials.  
I have chosen this article because it proposes a scientific analysis of the machine, taking into account the forces to apply.   
The two main forces that must apply for the constructions are the **shear** and **tension** forces.  
In particular the formula for the shear force applied by the blades is given by:

\[  F= 0.6 \cdot UTS \cdot \epsilon \sigma t \frac{T^2}{2 \cdot tg\phi}    \]

I made some research in order to explain each term:

*  Ultimate tensile strength (UTS), often shortened to tensile strength (TS), is the maximum stress that a material can withstand while being stretched or pulled before breaking.
The UTS value of four plastics is expressed in table:

![table_four_plastics](../images/final_project/shredding/UTplastic.png)  
*Table from the reference [^1] on the UT of different plastics.*  

* **t** is the material tickness
*  \(\phi \) is the angle between the object and the Blades
* T is the T-stress and is defined as a force which applies on a material, that the reference[^2] resumes as:
 >"A stress parallel to the crack faces at the crack tip. It is a useful quantity, not only in linear elastic crack analysis but also in elastic-plastic fracture studies."

* The quantities \(\sigma\) and \(\epsilon\), are respectively the stress applied and the strain.
The strain is defined as[^3] :

\[ \epsilon = \frac{\Delta l}{L_0} \]

, where \(\Delta l\) is the elongation.  
As a matter of fact, in a particular temperature regime, it follows the linear Hooke’s law:

\[ \frac{F}{A}=\sigma=E \epsilon \]

, where E is the proportional constant called Young's modulus.
The units of measurements are then

 \(  [\sigma] = [ \frac{N}{m^2} ] = [Pa]  \)


and \(\epsilon\)  is dimensionless from its definition.

Other important physical elements to design are the torque of the machine and the power of the motor.
The **torque** is defined as:

\[T=\textbf{d} \wedge \textbf{F} \]

, where **d** is the vector which indicates the point of application of the force **F**.

The **Torque of the motor** is the first element: it is defined as the torque which is needed to turn the engine and it is estimated through energy conservation where \(E_{tot}\) is computed considering the sum of the energy of rotation of the two engines defined as \( E_r=\frac{1}{2} I_i \omega_i^2 \), where \(I_i \) are the inertia momentum and \(\omega_i\) the frequencies.  
The torque T is then defined as:

\[ \sum_i T= I_{tot} \alpha  \]

where \(I_{tot}\) is the total inertia momentum of the system and \(\alpha\) is the angular acceleration.  
Starting from the definition of T, the **power to supply** is defined as:

\[ P= \frac{T \cdot 2 \cdot \pi \cdot n}{60} \]

where n is not specified in the article but I supposed it to be the number of teeth for the blades.


The [site of precious plastic](https://preciousplastic.com/) proposes a tutorial to build [the plastic shredder](https://community.preciousplastic.com/academy/build/shredder).  
Precious Plastic is an open-source project that was started in 2013 by Dutch designer Dave Hakkens.
The greatness of this project is the possibility to share knowledge about the machines and the fact that they can be built by literally everyone.  
The publication of the tutorial on the site has opened the world for new starts-up which were founded to take care of plastic recycling.
The main objectives of the project were to:

* Build a functioning shredding machine for treating plastics waste minimizing the costs.
* Improve step-by-step the machine in order to simplify the assembling process and its performances.


I finally decided to consult the article "Aryani, Nuri, Dede Buchori, and Albertus Budi Setiawan"[^1], which gives an estimation on the value of the power for a shredder.  
In this case equations are really similar and the power required is \( P \sim 4 kW \).

## References
[^4]:[Precious plastic site](https://preciousplastic.com/solutions/machines/pro.html)
[^1]:Aryani, Nuri, Dede Buchori, and Albertus Budi Setiawan. "Design of a Plastic Shredder Machine." IPTEK Journal of Proceedings Series 3 (2019): 35-39.
[^2]:[Mit site](https://abaqus-docs.mit.edu/2017/English/SIMACAETHERefMap/simathe-c-tstress.htm)
[^3]:"Harper, Charles A. Modern plastics handbook"
