# The printing process



In the precedent steps of the project I have obtained different kind of filament, which differ in particular for diameter and level of homogeneity.
The last section of my project is then dedicated to the printing process.



## General overview of the 3D printer

The printer, which has been used for the test, is a **Prusa slicer MK3S** which can be easily chosen in the menu of the Prusa slicer software, which can be downloaded from [this link](https://www.prusa3d.com/prusaslicer/).


![printer used](../images/final_project/printing_process/printer_setup.png)  
*Screen of the settings from the Prusa software.*  

Several challenges should be confronted in this case:

* The Prusa could not accept the filament proposed because of its diameter.
* The printing process could fail due to a modification of the properties of the material after the second extrusion.
* The inhomogeneity of the filament could create problems in the printing process. In particular the irregular flow of the material, by the printer, could cause some issues.

### The filament and its diameter

The producer of the printer, Prusa, merchandises a filament with diameter 1.75mm, which is bought at the interior of the FabLab ULB.  
The characteristics of this filament can be easily found scanning the QR code on the coil.  
An example of the data-sheet can be found [here](https://prusament.com/materials/pla/).  
An important element of analysis is the diameter of the filament, which is usually stressed out in the demonstration videos of Prusa.    
As shown in the youtube [video](https://www.youtube.com/watch?v=80hutqAJg-c&t=36s), provided by Prusa, the filament is manufactured with precision of \( \pm 0.02 mm\) on the diameter.

The diameter of the filament, obtained in the precedent section, has been measured in a range of:  

\[  d \in (1.40,1.70)mm \]

The last two weeks, dedicated to the printing process, have enabled my project to answer an important question:  
<span style="color:green">*Can we print with the recycled filament?*</span>


I have then tested many diameters of the filament in order to test which was accepted.
Finally, after some tests I can make some considerations:

* The filament is accepted with a diameter up to 1.40mm.
* The filament must be rectilinear or wrapped around a coil and great non-homogeneity should be avoided.
* Luckily, even though the diameter could not be constant, the printer can adapt the flow of material depending on the density.  
It helps to print object even though the filament is not perfect.


As a general consideration, the main principle should be applied:  
<span style="color:blue">*Advice*</span>: We must find a compromise between homogeneity and the diameter scale.  
In particular:
  d>1.40mm and excessive non-homogeneity avoided.

I have also discovered that the 3D printer regulates automatically its flow depending on the diameter of the filament.  
Therefore, in order to assure a uniform distribution of the material on the object, the flow velocity of the extruded(in the sense of the 3D printer) filament is reduced if the diameter is higher.   

## The anatomy of the 3D printer

One could use a 3D printer hours a day without never asking questions about how a 3d printer works or, for example, why the filament is accepted by the printer.  
In general, it is however a better strategy to go into depth.  
Here I interested in what is the mechanism that allows the printer to capt the filament and how the printing process changes following the filament.  
A first point of interest is <span style="color:yellow">the functioning of a general 3D printer</span>.  
The Prusa conception and particular notions on the sensors of diameter will be treated in the next sections.

The first source that has been consulted is a specialist site on 3D printers, which proposes an analysis of the following figure[^3]:  

![3d printer](../images/final_project/printing_process/AnatomyOf3DPrinter.png)  
*Image of a 3D printer model of reference. The different pieces of the printer have been resumed. Credits to [^3].*

The main elements are here resumed[^3]  [^4]:

* X-axis(1) - The left and right motions move along the X-axis which take place through an X-axis motor.
* Y-axis(2) - The forward and backward motions move along the Y-axis.
* Z-axis(3) - The up and down motions move along the Z-axis, with a motor in the back-end.
* Print Bed(4) - This is the surface where the print will be built on.
* Controller(5) - This is the brain of the printer. It is usually where interface for controlling the printer is and where all the other parts are plugged into, with a LCD screen in order to control the system.
* The Extruder(6), a fundamental part  - This is how the filament gets pushed into the nozzle for printing. There is a motor with a gear that turns and slowly pushes filament into the Hot-end.
* The Hotend(8), where the plastic melts so it can be deposited onto the print.   
The hotend is comprised of a few parts:
    * Nozzle - The nozzle gets hot and melts the filament. The nozzle is connected to the heater block. It comes in many sizes ranging from 0.1mm to as much as 2mm or beyond depending on the application. The typical size is 0.4mm. The nozzle can be swapped out for other sizes as needed.
    * Heater block - The heater block is where the heater cartridge is connected.
    * Heater Cartridge - The heater cartridge runs through the heater block as the source of heat for the hot-end.
    * Thermistor/Thermocouple is positioned just inside the heater block and reads the temperature of the hotend.
* Hot end - Heat Sink / Hot End Fan - This ensures that heat does not travel up the plastic and melt it prematurely before it reaches the nozzle. This phenomenon is called heat creep and it causes jams, especially with PLA.
* Cooling fan - The cooling fan cools the heat break.
* Part cooling fan, which quickly cools the material that has just been deposited.  
The **PLA** filament benefits greatly from being cooled quickly as opposed to ABS filament which may warp if cooled too quickly.
* End stops/Limit switches(9) to set a limit to the movement of the axis.
* Filament sensor, which is the main point of interest because it detects when the filament runs out and pauses the print.
In the case of the recycled filament, it could be a difficulty to overcome because, if the diameter is not wide enough, the printer could not capt the recycled filament.

This last section has provided some important information to introduce the printer.
Nevertheless, it is now important to focus on the specifics of the printer I used, the Prusa slicer.
I have not been able to found a general description of the printer by Prusa, instead important information have been found in the official site, in the Prusa community and the section of problem solving.

### Distinction between a Prusa model s(IMK3S) and not(IMK3)

A first point has been the distinction of the Prusa model depending on the kind of filament captor.
The Prusa model IMK3(s) uses a optical sensor in order to capt the filament.

The **optical filament sensor** detects the presence of a filament and also its movement.  
The modern version of the Prusa allows to stop automatically the printer if no filament is recognized.
This implies, as in a standard printer, the need to change the filament in order to continue the printing.

When it is the case, the new filament should be inserted correctly, as shown in the next figure.

![3d printer_nos](../images/final_project/printing_process/filamenttip.png)  
*Illustration on the good manner to insert a new filament. Credits to [^6].*

Some false alarms can generate if the following conditions does not take place:

* Keep the surroundings of the filament sensor clean
* Avoid "extreme" light conditions near the printer

In that case, the printer does not recognize the filament.
The Prusa model with the sensor s has been described in the following source[^5], and applies a new IR sensor.    
The new IR filament sensor still uses an optical sensor but is triggered by a mechanical action designed into the new plastic parts of the MK3S, MMU2S, and MK2.5S extruders.  
In particular, the function of **autoload** has been added [^5].  
<span style="color:blue">*Definition*</span>: Autoload is a function that, if the hotend is heated, will start the loading procedure once filament is detected.  
This can be turned ON and OFF from Settings -> Autoload, on the LCD-panel.  

During my experience, I have hold the autoload activated.  

The following reference helps to solve troubleshooting on the sensor and it has been useful for my project[^8].

The IR sensor is a hybrid solution that combines an IR laser sensor triggered by a tiny mechanical lever.
I have discovered that part of the problems I had experienced were not due to my filament but they were mostly due to common issues shared by the community.
In particular, the autoload was not working with my filament.
The common cause are[^8]:

* The new IR Filament sensor requires the S-version of the firmware. If it is not updated, as it was the case, it can cause some issues in the calibration.
* You should take care that the cable of the sensor has the following position:  

![good_sensor](../images/final_project/printing_process/good_sensor.png)  
*Good configuration of the sensor in the IMK3(s) case. Credits to [^8]*

* The FS lever, the part that is moved by the filament to activate the sensor, could not move correctly.
The reference gives important suggestion on how to manage this problem[^8].
* Clean up of printed part, which must be black coloured and in PETG, around the lever.







## The first test on the printer with the recycled filament

During the fourth week of the project I had already obtained a filament.  
Consequentially, I tried to print with it.  
Although, the printer **did not accept** the filament probably because it was too thin, which could cause a problem to the sensor.
Nevertheless, it could be associated to some issues on the sensors, as I have described in the precedent section.  

![printer used](../images/final_project/printing_process/diameter_comparison.jpeg)  
*Example of the kind of filament I had, compared to the virgin PLA filament.*

In particular, when loading the filament the system asked if the good color was extruded without seeing nothing on the plate.
As a consequence, I worked back to the extruder in order to improve the thickness and the homogeneity of the filament.


## The second test on the printer with a thicker filament

As long as I experienced with the extruder, I understood the mechanism and improved the thickness of the filament.  
In particular, depending on temperature of extrusion, I have extruded filaments with different diameters in a range of \(  d= 1.46mm; d= 1.52mm; d=1.60mm\).
These filaments could be used to print with the Prusa, under the condition of homogeneity.  
Moreover, it must be stressed out that a filament with a little section has not produced a right printing because the printer blocked at a certain time when the filament was not capt.  


The second try has not been completely successful, but still satisfactory enough.
The filament has been accepted by the printer, as it can be shown in this little video.  

<video controls muted>
<source src="../../images/final_project/printing_process/filament_accepted.mp4" type="video/mp4">
</video>

The first model I tried to print has been **the out of plane flexlink** that I had designed in the module3.  


![printer test1](../images/final_project/printing_process/outofplane_recycled.JPEG)
*Here I show how the base was posed on the plate of the 3D printer.*  
The final result has been the following:  

![printer test1](../images/final_project/printing_process/recycled_outofplane.JPEG)
*Printing result of the out of plane flexlink with recycled PLA.*  

![printer test1](../images/final_project/printing_process/outofplane_bad.jpeg)
*Printing result of the out of plane flexlink with virgin PLA.*

The result allows to identify a diminution in density of the flexlink, even though the G-code was the same.  
Some considerations can be done comparing the two figures:

* It was the first object I printed with the recycled material and it was one of the more complex objects to print.
* The Gcode of the file did not have the right condition of printing, due to the lack of support, and it already created some difficulties with the virgin PLA.
The comparison with the form obtained in the virgin PLA shows that the recycled filament is less compact because the structure is less dense than the other one.
Nevertheless, it must be underlined that the spiral process has not been followed in this case.
I should have started from a more simple design, as I will do in the next sections.

## The successful printing process with the recycled filament

The final step for my project has been the printing of two objects, the 3D benchy and the FabLab logo, using a recycled filament of PLA.

For comparison, the same object has also been printed using virgin PLA.  

### 3D Benchy
My teacher has advised to print a classic of the FabLab, the 3D Benchy, which is an easy printable object that allows to observe the difference between the different materials.  
As a matter of facts, it allows to compare different printing materials because, being a little boat, it requires quality of the material in order to print the details.
For example, as it can be shown into [the link of the Fabacademy of my professor Denis Terwagne](http://archive.fabacademy.org/archives/2017/woma/students/238/assignment5.html), it is also used as a model to compare different kind of 3D printer.

The stl file for the 3D Benchy has been downloaded from [the following site](https://www.thingiverse.com/thing:763622) and it can also be find in my repository for the project at [the following link](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-experiments/students/simone.vitale/-/tree/master/docs/project/3d_printing_file)[^1].
In this section we analyse the difference of the printing for the Benchy.

I add here a little video of the printing process:  

<video controls muted>
<source src="../../images/final_project/printing_process/boat_printing_process.mp4" type="video/mp4">
</video>

Here I show the recycled Benchy printed on the plate.

![printer test1](../images/final_project/printing_process/boat_plate.JPEG)  
*3D Benchy from the recycled filament of PLA.*

A comparison between the two printing is showed here:

![printer test2](../images/final_project/printing_process/boat_comparison.JPEG)  
*Comparison between the two 3D Benchy. The black one has been obtained from virgin PLA.*

It can be noticed that the recycled one seems less dense and it is also less compact.  
Moreover, it can be seen that at some point on the side the green boat presents a detachment of the plastic filaments.  
Nevertheless, the quality of the printing is very good.  
This boat has given the idea for my official logo, which I show here again.  

![logo](logo.jpg)  
*Official logo of the project, designed by Christian Fuduli, an italian architect of the University of Roma3 in Rome*  

**The message** is that, starting from some wastes, the recycling process allows to create something extraordinary as a boat.


### FabLab logo

The second file I printed has been a logo of the FabLab, which has been downloaded from [the following site](https://www.thingiverse.com/thing:2963172) and can be also found in the repository[^2].

Here I show the logo on the plate after the printing:  

![printer test1](../images/final_project/printing_process/fablab_logo.JPEG)  
*Official logo of the FabLab printed from recycled PLA.*


![printer test2](../images/final_project/printing_process/logo_comparison.JPEG)  
*Official logo of the FabLab printed from virgin PLA.*

It can be noticed how the printing process is efficient with the recycled filament.  
Nevertheless, some details around the borders of the object, sort of little holes, suggest that some improvements could be made to the filament as expected from the start.    



## Final considerations

As a final consideration, I have noticed some differences between the two products:

* Even though the same G-code has been used, the printed object from recycle filament results less dense as if the extrusion of the 3D printer has not been perfectly executed.
It could undermine the quality of the printing because some holes in the structure could modify the forces on the object.
* The quality is optimal for printing little objects, in particular the material is solid.  
 It can be identified some filaments residuals which show a lack of precision.
* A future project could include the realization of the fan in order to improve the diameter of the filament and, hence, the printing.
* I could observe that the moment where the filament looses uniformity, the 3D piece is less dense and some little holes can appear.

All these considerations should be understood to be some suggestion for the future people which, I hope, will join my project.

### Points of improvement for this part

The main points of improvement regards the previous part of the project.  
This part will easily follow when the perfect filament will be produced.  
Moreover, a deepen analysis could be made on the pieces in order to measure the differences in terms of elasticity, density and mechanical properties.











## References
[^1]:[Reference site for the benchy](http://www.3dbenchy.com/download/)
[^2]:[Reference site for the FabLab logo](https://www.thingiverse.com/thing:2963172)
[^3]:[Anatomy of a 3D printer: 3D printer universe](https://3dprinteruniverse.com/blogs/world-of-3d-printing/anatomy-of-a-3d-printer)
[^4]:[Anatomy of a 3D printer: matterhackers](https://www.matterhackers.com/articles/anatomy-of-a-3d-printer)
[^5]:[Reference from Prusa site on s sensor](https://help.prusa3d.com/en/article/ir-filament-sensor-mk2-5s-mk3s_112214)
[^6]:[Reference from Prusa site on non-s sensor](https://help.prusa3d.com/en/article/filament-sensor-mk3-non-s_2125)
[^8]:[Prusa tutorial to solve problem on the sensor](https://help.prusa3d.com/en/article/ir-filament-sensor-troubleshooting-mk2-5s-mk3s_112226/)
