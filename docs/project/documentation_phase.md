# The documentation phase

An important task of my project has been to document about different elements:among them **the quantity of the plastic wastes** and **the concept of plastic**.  
These have been the foundation of this section.

## Documentation on the availability of the material
The first part of the documentation phase has been the interaction with the managers of the FabLab ULB in order to obtain plastic to recycle.  
I have been furnished with 3 boxes of PLA wastes from 3D printing, which had been created in a precedent course at the FabLab.

![boxes](../images/final_project/documentation_phase/fablab_plastique.JPEG)  
*Photo of the materials that has been furnished by the FabLab ULB, the amount of material comes from about 3 months of 3D printing.*  
In the boxes there were only a material, PLA.
This is an important constraint in order to assure a correct process of extrusion.  
The processes, such as the extrusion, could be dangerous if different plastics would be together because a part of them could burn releasing toxic polluting having different melting temperatures.

## Documentation on plastic, a deepened knowledge of a well-known material

The first step has been to document about the role of plastic recycling, basing on the European report [^1].  
In the report a sentence has attracted my attention :  
>The production of plastic has grown exponentially in just a few decades - from 1.5 million of tonnes in 1950 to 322 million of tonnes in 2015 worldwide – and with it the amount of plastic waste.

The plastic use would be for the human population less damaging for the environment if the recycling cycle functioned well and a circular economy was acted.
The main inconvenient for plastics are the following:

* Made from nonrenewable crude oil resources.
* Give off toxic fumes when burned.
* Non-recyclable materials are difficult to re-purpose and do not decompose in landfills.

Plastic is strongly present in our homes, and the fight against plastic pollution does not literally mean to ban plastic.  
Although, the aim is to reduce its use or recycle it, especially where it is possible.  
We could think that using plastic means to know how it is made, however I will try to change your mind in this section.  
A deepened knowledge of the material is a fundamental ingredient to realize something with it.    

Therefore, let's first analyze <span style="color:red">what is plastic</span>, through different sources.
Among them, I have chosen to divide between scientific sources, in particular scientific articles and books, and industrial documentation of plastic producers.

This topic has grouped the following questions from the class, which I have answered in this module.  
![start](../images/final_project/documentation_phase/plastic_whatisit.JPEG)  
*Questions that the class has proposed about plastic during the brainstorming*



Plastic is a kind of polymer.  
A polymer is a chain of small molecules, which are replicated in order to give birth to a compound.
In particular plastic is a synthetic polymer, composed of elements as Carbon(C),Hydrogen(H) and other elements.[^2]

The sub-units of polymers are called monomers and, depending on the form and the length of the chain they form,
different mechanical properties will be associated to the polymer[^14].


A more precise description on plastic has been found in the book "Harper, Charles A. Modern plastics handbook. McGraw-Hill Education, 2000.", which is a must in this field[^3].

>Plastic is the union of unit structures, monomers, which are associated together to form polymers.  
A polymer is prepared by stringing together a series of low molecular weight species (such as ethylene) into an extremely long chain(polyethylene), an analogy can be made with the DNA chain.  

A first distinction on plastic can be made between **thermosets** and **thermoplastics**.

We will describe plastic as a visco-elastic fluid, which means that both the properties can be associated depending on its temperature.  

At high temperatures, in thermoplastics, the chains of polymers tend to break and plastic becomes a viscous fluid.
On the contrary, a thermoset has all of the chains tied together with covalent bonds in a network (cross-linked) which does not allow this kind of phenomena.  

This table shows the main differences between the two categories.
![structures](../images/final_project/documentation_phase/table_thermoplasticandthermosets.png)  
*Table which illustrates the different properties of plastics, credits to [^4]*

>A thermoset can not be reprocessed being cross-linked, but a thermoplastic material can be reprocessed by heating to the appropriate temperature, which makes the second one recyclable.  

The following figure clearly shows the difference between the two structures:  
![structures](../images/final_project/documentation_phase/structures.png)  
*Different structures of polymers, from [^3]*.

Therefore, the kind of chain and the chemical properties of the elements will determine the total properties of the thermoplastic.  
The more a polymer has a regular structure, the more is crystalline.  
On the contrary, it is described as anomalous if disordered.


### Thermoplastics, particular application on PLA

Thermoplastics, such as polyethylene(PE) and polystyrene, are capable of being molded and remolded repeatedly.

The plastic development and conception has been started from 1900s.
Before that period, rubber was used as the principal material.
A plastic differs from a rubbery material due to the location of its glass transition temperature (\( T_g \)), defined as the temperature temperature at which 30–50 carbon chains start to move.  
It implies than that the components of the polymers chain start to move as the material tends to stretch.
A plastic has a \( T_g \) above room temperature, while a rubber will have a \( T_g \) below room temperature[^3].
More information about the glass temperature can be found in the article[^4].
Here it is a typical curve for a plastic material, where different regimes can be identified.  

![curve_plastic](../images/final_project/documentation_phase/curve_plastic.png)  
*Qualitative representation of the temperature(x-axis) in function of the logarithm of the storage modulus G, credit to [^3].*

Here it follows a brief description of the regimes:

* Plastic at low temperature is described as a glassy solid.
Glassy behavior in a material is characterized by a very long relaxation time.  
When a soft material is subject to a deformation, the material will respond viscoelastically, then return to an
equilibrium state over some characteristic timescale known as the relaxation time \( \tau \)[^14].

The law which characterizes the  glassy state is the Hooke's law:

\[ \sigma= E \epsilon \]

, where \( \sigma \) is the stress being applied and \( \epsilon \) is the strain.  
The strain is defined as:

\[ \epsilon= \frac{L-L_0}{L_0}\]

, where L is the length of elongation and \( L_0\) is the initial length.  
The Young’s modulus, E, is the proportionality constant relating stress and strain, and is measured in Pascal.  
It can be shown[^15] that E is related to G, in figure, via the relation[^15]:

\[   G= \frac{E}{2(1+\nu)}  \]

where $\nu$ is the Poisson coefficients which varies from the material, which measures how a solid contracts on the y,z directions(for example) when a constraint on x is applied to it.
The modulus E, it  can vary from 1 \( \frac{MN}{m^2} \) to 50 \( \frac{MN}{m^2} \) .

The temperature at which the polymer behavior changes from glassy to leathery is known as <span style="color:blue">the glass transition temperature</span> \( T_g \).
The law which relies \(\sigma\) and the variation of \( \epsilon \) is the constitutive law:

\[ \sigma= G \dot\epsilon t = \frac{\mu}{L} \frac{\partial l}{\partial t}  \]

, where  \( \dot \epsilon \) is the rate of elongation deformation.  
\(  \sigma \) is the constraint applied on the liquid flow, \(  \mu \) is the dynamic viscosity ( \( [µ] = [Pa][s] = [N][s][m]^{-2} \) ).

* The rubbery plateau is followed by the rubbery flow, where deformations of the chain become nonrecoverable.
* Finally, raising up the temperature, another linear regimes appears:

\[ \sigma= \mu \dot\eta = \mu \frac{v}{d}  \]

, where  \( \dot \eta \) is the shear rate.  
\(  \sigma \) is the constraint applied on the liquid flow, \(  \mu \) is the dynamic viscosity.
From these relations it is possible to derive the definition of time of relaxation \(  \tau_{\nu} \) for a viscoelastic material as:

\[ \tau_{\nu}= \frac{G}{\mu}  \]

It defines how much time is needed to have the fluid characteristics.  
A fluid can be thought as the union of horizontal constituents that slides on one another, and the viscosity indicates the resistance to the sliding.  
![curve_contraint](../images/final_project/documentation_phase/viscoelastic.png)    
*Figure of the contraint in terms of the time of the system, credit to [^16].*  
At this stage plastic corresponds to an highly viscous liquid that can be shaped using plastics processing equipment, such as the extruder.

The micro structure of the material has a strong influence on the properties.

#### A focus on PLA
A specific graphic can be provided for PLA, which is the material that is going to be recycled.
It is really useful in order to understand in which regime we are working.  

![curve_plastic](../images/final_project/documentation_phase/storage_pla.png)  
*This graphic shows the storage modulus(G) in function of temperature(T) for PLA with different temperature of creation during its first extrusion.[^5]*


It helps us identify a regime between 150°-175° for the extrusion because \( T_g\sim 80° \)
and \( T_{molding}\sim 164°-204° \) depending on the temperature of the producer of the filament.
The Prusa site also provides data-sheets where all the information on the produced PLA can be found[^6].
Here I report two important data to remind of:

* Melting point/melting range: 150-180°C
* \(T_g\)(Glass Transition Temperature): 55-60°C

PLA has a semi-crystalline structure and is a biodegradable material.

Here we recall two important definitions:  

*  The Melting point is the critical temperature above which the crystalline regions in a semi-crystalline plastic are able to flow and demonstrate fluid behavior.
* Plastic materials are made up of uneven chain lengths and require different amount of energy to move, which means that \( T_g \) or \( T_m \) for semi-crystalline polymers is not one definite temperature but a range of temperature during which all the chains start to move and experience complete flow.[^5]

At temperature above melting point, semi-crystalline plastics exhibit a rapid phase change from solids to viscous liquids that can be molded or given shapes and the semi-crystalline materials continue to maintain their mechanical properties until the temperature increases to reach \(T_m\).[^5]





## The quest for my project

Having answered at the question on which material could be recycled, here it came the following question: how to develop a recycling method of the filament at the interior of the FabLab.
I then thought to use this project in order to pose the basis showing that the printing with the recycled material is possible.

An important source of inspiration has been the article:
![start](../images/final_project/project/state_article.png)  
*Screenshot of the article [^9]*

This article explains how printable filaments can be made from a variety of thermoplastic materials, including those from recycling.  
The recycling process applies on the wastes from the 3D printer.
The filaments used in 3D printing are primarily thermoplastics and the most popular are acrylonitrile butadiene styrene (ABS) and polylactic acid (PLA).  
However, 3D printing generates large amounts of waste, which are the result of failed prints or rejected support structures.    
In figure it is shown the cycle of a plastic waste in general.  
![cycle_of_filament](../images/final_project/documentation_phase/cycleofplastic.png)  
*Plastic cycle, from the reference [^9]*

The following image gives an example of the plastic cycle in the case of the 3D filament.  
![cycle_of_plastic](../images/final_project/documentation_phase/cycleoffilament.png)  
*Scheme to describe the cycle for a plastic involved in a 3D printing process, from the reference [^9]*
### A focus on the properties of the recycled material

The process of extrusion, in which the formation of the new filament takes place, involves processes of shear stress, temperature, and oxygen that could degrade polymers.  
The change of the physical properties of the polymer significantly influences the obtaining of high-quality extrusion products.  
Multiple extrusion of polymers has a strong influence on their change in viscosity, molecular weight, and breaking strength.  

The PLA, even though bio-based, has a disadvantage because it is highly sensitive to elevated temperature (~ 200 °C).  
Moreover, the recombination of polymers takes it to an increase of the melt flow rate, which imples issues in the 3D printer.  
A research has shown that degradation increases up to 30% after 3 cycles and 60% after 7 cycles.[^9]  
Additionally, significant decrease in the viscosity has been observed in the case of washed PLA wastes, which results from influence of high temperature and shear stress during the polymer reprocessing.  

A good solution is the addiction of virgin PLA to the recycled and shredded PLA filament, because it significantly improves the viscosity of the blend as well as its mechanical and thermal properties.  

A source has identified a degradation of PLA after 5 recycling processes[^12].  
The most important changes are given by the recombination of the chain of polymers, because the process of extrusion heats thermoplastic and it causes a breaking of the different chains of polymers(T=200C°) with a successive recombination when composing the filament.  



The following table resumes the different merchandiser of 3D filament from recycled materials:  
![recylingentreprise](../images/final_project/documentation_phase/recycledfilamententreprise.png)  
*Table from the reference [^9]*

An interesting project, which is cited in the article and directly aims the **Steamlab** is called _The Perpetual Plastic Project_.  
It aims to transmit the values of recycled plastic converting plastic waste into new useful products using 3D printing technology.   
Every information is on [the official site](https://www.perpetualplasticproject.com/?lang=en).


The main problem is that, actually, the price of recycled filament is really similar to the new filament.  
This usually creates a tendency to buy virgin plastic.  
Nevertheless, these chooses are really important for the planet because degradation of plastics lasts from 10 to 450 years and burning it can not be the solution.  
The idea to recycle directly at home and from the consumer's own material is prodigious.  
As a matter of fact, this provides several savings: environmental(for the pollution) and enables to close the loop of circular economy.  













The article "Fonda, Carlo, and Enrique Canessa. "Making ideas at scientific fabrication laboratories." Physics Education 51.6 (2016): 065016." also speaks about the necessity to obtain low-cost plastic material for Fablabs, in particular for 3D printers[^11].



Another important reference that analyses the need for recycling improvement or re-use of plastic affirms that 710 Tonnes of plastic will be injected up to 2040 in the environment.[^10]  
The article also mentions the scenarios for plastic and micro plastic based on a computational model.  
Moreover, it also mentions different solutions which could be acted to implement the recycling process.  
It is really interesting and its description in depth can be found in the Appendix.  
The article however underlines that the most important difficulties resides in the connectivity of the different municipal centers for recycling and the difficulty to uniform the resources of the different countries.  
In fact, economic difficulties would always be a tendency for plastic use and the weak system of recycling in these countries could  worsen the situation.  
This is why I see the FabLab as a great opportunity to guide the populations through knowledge of recycling processes and it would help to uniform the different countries.  
A first signal should be given by the FabLab in European countries, and in particular the FabLab Brussels.







### FabLab and the European Union, perspectives for future projects

The European Union is involved in the financing of the FabLab and asks for perspectives in the recycling process.  
The **"Digital Fabrication Labs Handbook"**, provided by the FabLab, gives some insight of the perspectives.   
A section is dedicated to the **precious plastic project**, which could be linked to the FabLab thanks to the convergence of ideas about the frugality of projects.  
Some ideas are proposed for circular economy:

* The first project, the 3D fuel, proposes 3D filament starting from organic material(and in particular wastes).
[Their site](eu.3dfuel.com/products) proposes some interesting filament produced from coffee or garbage.
* Another interesting element of analysis is the recycling of plastic for 3D printer filament.  
The most important references in this case are [refilament](https://verycreate.com/refil-re-filament/) and [filamentfive](https://www.filamentive.com/), which are merchandiser of recycled plastic filament.
Therefore, a first choice could be to buy from this kind of market in order to reduce \( CO_2 \) emissions.

















## Appendix

Here the curious reader can found some additional researches I made and which are not strictly useful for the project.

### Examples of Thermoplastics

Thermoplastics, which are present in the Fablab for example, are: [^7][^8]

* Polyethylene terephthalate (PETE or PET):
PET is the most widely produced plastic in the world.  
It is used predominantly as a fiber (known by the trade name “polyester”) and for bottling or packaging.  
For example, PET is the plastic used for bottled water and is highly recyclable.  
These plastics are easily recyclable and PET plastics are recycled into carpet, furniture, and fiber for winter garments.
* HDPE (High Density Polyethylene):  HDPE is the most commonly recycled plastic and can be reused safely.  
It is nowadays common in the food industry for the robustness of the plastic. Recycled HDPE is made into pens, plastic lumber, plastic fencing, picnic tables and bottles.
* Polyethylene (PE):
There are a number of different variants of polyethylene, mostly used as plastic containers.
* Polyvinyl Chloride (PVC):
Polyvinyl Chloride is perhaps most well known for its use in residential and commercial property construction applications.
Different types of PVC are used for plumbing, insulation of electrical wires, and “vinyl” siding.
Therefore, it is found in the vinyl cutter materials.
Specialized programs recycle PVC into flooring, paneling and roadside.
* Polypropylene (PP):
Polypropylene is used in a variety of applications to include packaging for consumer products, plastic parts for the automotive industry, special devices like living hinges, and textiles.
It is semi-transparent, has a low-friction surface, does not react well with liquids, is easily repaired from damage and has good electrical resistance.  
When recycled, it’s turned into heavy-duty items like pallets, ice scrapers, rakes and battery cables.

* Polystyrene (PS):
Polystyrene is used widely in packaging.
 It is also available as a naturally transparent solid commonly used for consumer products like soft drink lids or medical devices like test tubes.

* Polylactic Acid (PLA):
Polylactic Acid is unique in relation to the other plastics on this list in that it is derived from biomass rather than petroleum. It biodegrades much quicker than traditional plastic materials.  
Therefore, it is one of the most important material to study.  
It is mostly used for 3D Printing.

* Polycarbonate (PC):
Polycarbonate is a transparent material known for its particularly high impact strength relative to other plastics.

* Nylon (PA):
Nylon is used for a variety of applications to include clothing, reinforcement in rubber material like car tires, for use as a rope or thread, and for a number of injection molded parts for vehicles and mechanical equipment.  
 It is often used as a substitute for low strength metals in applications like car engines because of its high strength (relative to other plastics), high temperature resilience, and high chemical compatibility.
* ABS (Acrylonitrile Butadiene Styrene):
ABS has a strong resistance to corrosive chemicals and physical impacts.   It is very easy to machine, is readily available and has a low melting temperature making it particularly simple to use in injection molding manufacturing processes or 3D printing.
For example, Lego Toys Made From ABS Plastic.

Other examples of polymers are the biodegradable polymers, which will be taken into account in some of the articles.  
The structure of the backbone strongly influences the kind of degradation for biodegradable polymers.
However, particular attention should be made on where the wastes are left because the Ph-level of the water could be modified, creating environmental problems.  
Two kinds of degradation are possible:

* Chemical degradation includes degradation by oxidation, photo-degradation, thermal degradation, and hydrolysis.
* Microbial degradation can include both fungi and bacteria.  
The susceptibility of a polymer to bio degradation depends on the structure of the backbone.  
An example is given by PLA.


### An insight into the future scenarios on the recycling of plastic

The following reference: "Lau, Winnie WY, et al. "Evaluating scenarios toward zero plastic pollution." Science 369.6510 (2020): 1455-1461" is deepened in this section.[^10]    
I have chosen this article in order to get some ideas about waste treatment of plastic and eventual solutions applicable in the FabLab.  
In particular, I have seek to answer the question about how to create a recycling cycle in the Fablab and how to reduce wastes.  


The interesting part of the article is the analysis of microplastic, which are rarely taken into account, and the simulation which have been effectuated in order to predict the quantity of plastic.  
The scientists that wrote the article have elaborated a model, based on ordinary differential equations, that allows to simulate, throughout Monte Carlo simulations, the rate of plastic(macro and micro) which will be injected in the environment.  
The basis of the computation have been some municipal structures and data from a marine observatory for microplastics.  
"8 system of intervention are proposed by the authors in order to simulate the different scenarios:

* (i) reducing plastic quantity in the system,
* (ii) substituting plastics with alternative materials and delivery systems,
* (iii) implementing design for recycling,
* (iv) increasing collection capacity,
* (v) scaling-up sorting and mechanical recycling capacity,
*  (vi) scaling-up chemical conversion capacity,
* (vii) reducing post-collection environmental leakage,
* (viii) reducing trade in plastic waste."[^10]  
The scenarios, which have been included in the model, are: "
* (i) ‘Business as Usual’ (BAU),
* (ii) ‘Collect and Dispose’,
*  (iii) ‘Recycling’,
*  (iv) ‘Reduce and Substitute’,
* (v) an integrated ‘System Change’ scenario that implemented the entire suite of interventions.  
"[^10]
The part where the different scenarios on plastic treatment are analyzed is really interesting because by 2040 the best solutions, which would reduce the plastic production of 78% is the "changing scenario", but what does it exactly mean?  
It means that a substantial reduction in mismanaged and disposed waste should be achieved through increases in the proportion of plastic demand reduced, substituted by alternative materials and recycled.  
Evidently, the application of plastic were it is necessary would be excluded from this no-plastic revolution, for example the medical use of plastic.



### Other insight of the main source [^9]

For the first time of my analysis, in this article, I have encountered the term of **first recycling** and **secondary recycling**, which answers the question about how many times a material could be recycled.    
They differ from the fact that the first one does not contain contaminants, while the second one should be decontaminated before being extruded.  
It suggest that in a way a piece of plastic can not be recycled at infinite but has a time after that is too contaminated.   

Another point were the article goes beyond the actual plastic compositions.  
 It tries to make some hypothesis on the **additive polymer manufacturing**, built in order to be environmental-friendly.  
The main focus of the research is to add additives to the chains in order to improve mechanical properties.  
 Examples of additives could be _carbon fibers_ or a  _dopamine solution_ (4h).    
 Moreover, new printing frontiers could be the mats, adding PET recycled.  
 _Biocarbon_ is another useful additive which can increase, for the PET recycled, of 32% in strength and 60% in elasticity.  
Nevertheless, these considerations were not suited for my project


### The state of the art of plastic recycling and the green economy


In order to give an insight about plastic recycling I have also consulted the article [^13], in which there is a focus on the recycling of macro fibers.  
I thought that this article could generate some interesting ideas as a concept to apply at the interior of the FabLab.  
As a start, it resumes the possible evolution of plastic recycling in Europe.  
In particular, in Europe at the moment only 25% of plastic is recycled, which is really far from the objectives of 55% for 2025.  
The article suggests that a part of the plastic could be used in a circular economy method for the construction industry.  
The macro-fibers of plastics are especially considered as composite material for steel reinforcement.  
The analysis which is provided, even though not clearly related to my questions, helps understanding the main physical properties to consider when classifying plastics.  
The main point of analysis are: the workability, the density and its durability because we need to assure a convenience in recycling plastic and we must assure how long could we recycle it.  


## References

[^1]:[The European official site](https://www.europarl.europa.eu/news/en/headlines/society/20181212STO21610/plastic-waste-and-recycling-in-the-eu-facts-and-figures)
[^2]:[Plastic producer](https://www.rspinc.com/blog/plastic-injection-molding/polymer-vs-plastic/)
[^14]:Hirst, Linda S. Fundamentals of soft matter science. CRC press, 2019.
[^3]:Harper, C.A., 2000. Modern plastics handbook. McGraw-Hill Education.
[^15]:Bower, David I. "An introduction to polymer physics." (2003).
[^16]:Course of the ULB university on Continuum mechanics, professor  Fabian Brau, 2021.
[^4]:Shrivastava, A., 2018. Introduction to plastics engineering. William Andrew.
[^5]:Bai, D., Liu, H., Bai, H., Zhang, Q. and Fu, Q., 2016. Powder metallurgy inspired low-temperature fabrication of high-performance stereocomplexed polylactide products with good optical transparency. Scientific reports, 6(1), pp.1-9.
[^6]:[PLA user manual by Prusa](https://prusament.com/media/2020/04/Prusament_PLA_SafetySheet_24042020_ENG.pdf)
[^7]:[Site of a plastic society](https://www.creativemechanisms.com/blog/eleven-most-important-plastics)
[^8]:[Site of a recycling society](https://www.generalkinematics.com/blog/different-types-plastics-recycled/)
[^9]: Mikula, K., Skrzypczak, D., Izydorczyk, G., Warchoł, J., Moustakas, K., Chojnacka, K. and Witek-Krowiak, A., 2020. 3D printing filament as a second life of waste plastics—a review. Environmental Science and Pollution Research, pp.1-13.
[^10]:Lau, Winnie WY, et al. "Evaluating scenarios toward zero plastic pollution." Science 369.6510 (2020): 1455-1461"
[^11]:Fonda, Carlo, and Enrique Canessa. "Making ideas at scientific fabrication laboratories." Physics Education 51.6 (2016): 065016
[^12]:Cruz Sanchez FA, Boudaoud H, Hoppe S, Camargo M (2017) Polymer recycling in an open-source additive manufacturing context: mechanical issues.    
Addit Manuf 17:87–105. https://doi.org/10.1016/j.addma.2017.05.013
[^13]:Vaccaro, Pietro A., et al. "A STATE-OF-THE-ART REVIEW ON CONCRETE WITH PLASTIC AND RECYCLED PLASTIC MACRO FIBERS." NEW TRENDS IN GREEN CONSTRUCTION: pages 53-54."
