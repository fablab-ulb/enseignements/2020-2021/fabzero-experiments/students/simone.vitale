# A guide through my project

This section of the Web page is dedicated to my final project for the FabZero experiment.

![fablab2](../images/final_project/project/fablab_ulb.png)  
*Image of the official FabLab logo [^9]*


![logo](logo.jpg)   
*Official logo of the project, designed by Christian Fuduli, an italian architect of the University of Roma3 in Rome*  
My project has been conceived to answer the question:  
#### <span style="color:green">What can WE do to improve the recycling process at the FabLab?</span>
This main section for the project introduces the main features of my project and helps guiding in the documentation I provided.

## The background of the project

I reflected about the FabLab role in the society as a boost to focus on the environmental question and improve the recycling processes.  
As a matter of facts, an important part of the FabLab materials is plastic-composed and, with the expansion of the FabLab net, the environmental question should be taken into account.  
Just to provide an example, in my 6 weeks I have clearly observed as plastic is massively used for its facility to build everything, in particular for **3D printing processes**.  
Moreover, other materials are plasticized, for example the vinyl in the vynil cutter.  
Therefore, assuming that plastic is necessary for many processes, I have tried to question about how the recycling process could be developed at the interior of the FabLab in order to neutralize the impact of the plastic consumption.  

There are two reasons why it is fundamental:

* The FabLab could give a good example, as a local recycling center, reducing the burden of the communal authorities in the waste treatment.
* The recent development of FabLab all over the world require that they should be environmentally-friendly in order to reduce their impact, as shown in the next figure.

![](../images/final_project/project/fablabs_map.png)
*Map, which shows the FabLabs all over the world, around two thousands[^10]*  

The formulation of my question has been started from a more general issue of the present, in particular:   
#### <span style="color:yellow">What could I do to solve the plastic problem?</span>
However my questions has been modified in order to focus on something realizable in the FabLab environment.

The issue was then transposed to global to local and the question has become:  
#### <span style="color:blue">What could I actually do to improve the Fablab recycling?</span>  
or, better,:  
#### <span style="color:green">What can WE do to improve the recycling process at the FabLab?</span>

The answer to the question is simple, reduce the FabLab wastes from 3D printing.
In order to do that, the Fablab has a machine for creating filaments starting from pieces of plastic from previous 3D printing.  
The process, sometimes complex, will be described in the following sections.


## A second life to the 3D printing filament


The project has been inspired by the article:  
![start](../images/final_project/project/state_article.png)
*Screenshot of the article from the reference[^2], which has been an important source of inspiration.*


Therefore, since the start of the project, I have dedicated myself to create a recycling process linked to the 3D printer at the interior of the FabLab and to improve the sensibility on this theme.  
The material that I have prepared during my 6-weeks project includes:

* A creation of a tutorial on what could be recycled of the failed printing, with PDF slides [^11].
* A tutorial on how to create a new filament.
* A description on how to print with the recycled filament.




## The requirements for the project

The mentor of the project has assigned some tasks to be accomplished by the project.

As a consequence, the project has been conceived with the following directives:

* I should design, fabricate and document a scientifically-based and frugal solution to solve a real-world problem.
* I had to position my project along the 17 Sustainable Development Goals listed by the United Nations[^1].
* I had to show scientific evidence with sources and references on which you build on your project.
* I had to use at least one digital fabrication technique that is used in a FabLab.

These four points will be treated in the following sections and, particularly, in the other sub-sections of the project.

### A frugal solution for the recycling process, the background

The frugal solution I conceived has been based on different phases that I have accomplished.
Before arriving to my final answer, I have spent a period thinking about which global question I would pursue.
This phase, called **the investigation**, has oriented me through environmentally question of plastic.

The second phase has been the **engagement** phase, in which I consulted [the Fablab tutorial](https://fablab-ulb.gitlab.io/enseignements/2020-2021/fabzero-experiments/class-website/fabzero-modules/project-kickstarter/) on how to find the right question.

A lesson of the course has been dedicated to the important phase of **brainstorming** of our projects.
The following experience has been proposed in order to allow the creation of ideas:

* Each member of the class has written on their paper the main question, in my case that was:  
 "**How can we improve the recycling process at the interior of the FabLab?**"
* The class has then spent some times reading the questions of the others and writing on some post-it questions or ideas that the question itself generated on them.
Here I show the starting point of my brainstorming.
![start](../images/final_project/project/starting_disposition.JPEG)
*Starting point of my brainstorming*

* We have been then asked to divide the questions in almost 4 topics and then to resume the questions of each topic in one question.

Therefore, I obtained the following interesting themes that, nevertheless, are too vast to treat in only one project:

  * 1:  What is the difference between the different plastics, in particular in the recycling process?  
  * 2:  What can we do with the recycle plastic at the interior of the Fablab?  
  * 3:    How to quantify the possible recycle of the FabLab with the actual methods, allowing a classification of plastic?  
  * 4:  How to diminish the plastic waste?  


In the documentation phase I then concentrated on **the first question** and **the second one** with particular reference to the mechanism of extrusion.





 Finally, the **act** phase has consisted in four steps that I have planned meticolously in order to respect the deadlines of my project.

 Here I add a table of content where four phases can be identified.






| Phase |  Description    |  Reference Link  |           Title           |
|-----|-----------------|---------|--------------------------|
| 1   | Documentation on the plastics that can be recycled from 3D printer wastes and organization of these wastes.    | [^3] |  [Documentation](https://fablab-ulb.gitlab.io/enseignements/2020-2021/fabzero-experiments/students/simone.vitale/project/documentation_phase/)  |
| 2   | Realization of the shredding of plastic starting from 3D wastes from the FabLab ULB.    | [^4] |  [Shredding plastic](https://fablab-ulb.gitlab.io/enseignements/2020-2021/fabzero-experiments/students/simone.vitale/project/shredding/)  |
| 3   | Realization of the process of extrusion at the interior of the FabLab, in order to obtain back a filament.  | [^5] |  [The fabrication of the filament](https://fablab-ulb.gitlab.io/enseignements/2020-2021/fabzero-experiments/students/simone.vitale/project/extrusion/)  |
| 4   | Printing process, from the recycled filament, and confrontation with the new filament with the differences between the two.   | [^6] | [The final printing](https://fablab-ulb.gitlab.io/enseignements/2020-2021/fabzero-experiments/students/simone.vitale/project/printing_process/)  |





These four steps have been documented in the subsections, which are organized with the essential information for my project, with several scientific references, and an appendix where the curious reader could consult specifics.
For example, for the shredding process I have used a shredder of the **Precious plastic** in Brussels and, even though I just used the machine following the directives, the mechanism of the shredder has been described in appendix to give an idea of the mechanical process.


### The sustainable goals of United Nations

My project is particularly suited in the United Nations objectives[^1].

![onu objectives](../images/final_project/project/onu_objectives.png)
*Figure taken from the official site of the United Nations, which resumes the 17 objectives[^1]*  
The main objectives that I cover in my research are:

* The <span style="color:orange"> \( 9^{th} \)</span> objective for the innovative view of the project in the objective to reduce the plastic consumption at the interior of the FabLab all over the world.

* The <span style="color:yellow"> \( 12^{th} \)</span> objective is clearly suited in my project because I could reduce the consumption and the production of plastics for the 3D printer.

* The <span style="color:green"> \( 13^{th} \)</span>  objective is clearly identified in the possibility to reduce \(CO_2\) emissions.
* The <span style="color:blue"> \( 14^{th} \)</span> objective on life below water is linked to micro plastics which could eventually be created from plastic disintegration.




### The digital fabrication techniques

My project consists in the recycling process of 3D printer wastes.
Therefore, the techniques I used are:

* The 3D printing process, which I described in details during my 6 weeks formation[^7].
I used a Prusa Slicer model i3 MK3S, which I have been already used in the learning weeks at the FabLab.

* The filament extruder, which allows to create a filament starting from a recycled material.


## The spirit of my project

The choice of my project has been inspired by some elements of quest that I cite here:

* It must be durable, hence description on how to upkeep the machines.
* It must be financially accessible, hence with a practical gain for the institution FabLab.
* There must be a clear question to be answered rapidly and clearly.
* The act of research must be its motor.

These principles have been transmitted to us by the researcher of the project value bugs[^8], during one of our courses.

These are the main machines that were needed for my project:

* The first machine is the plastic shredder.
* The second machine, which allows to make the plastic filament starting from little plastic fragments, is called extruder.

Some knowledge about plastic materials and which kind of plastic is good to recycle is required in order to be efficient.
This argument will be treated in the next part of the project, dedicated to the documentation phase.  

I would also like to underline the fact that the same process could be done with every other material at the interior of the Fablab.  
In fact, the ultimate aim of my project would be to allow people to recycle at home the wastes.


## <span style="color:white"> Acknowledgments </span>

I would like to thank all the extraordinary FabLab team for the precious help in my project.
An important person of reference has been my mentor and teacher, Denis Terwagne, who I thank for the support.

## References

[^9]:[FabLab logo](https://eur01.safelinks.protection.outlook.com/?url=https%3A%2F%2Fgitlab.com%2Ffablab-ulb%2Fenseignements%2F2020-2021%2Ffabzero-experiments%2Fclass%2F-%2Ftree%2Fmaster%2Fvade-mecum%2Fimg&data=04%7C01%7Csimone.vitale%40ulb.be%7Cc4664ca3c06f4389f81408d92d7a6a9f%7C30a5145e75bd4212bb028ff9c0ea4ae9%7C0%7C0%7C637590826065413055%7CUnknown%7CTWFpbGZsb3d8eyJWIjoiMC4wLjAwMDAiLCJQIjoiV2luMzIiLCJBTiI6Ik1haWwiLCJXVCI6Mn0%3D%7C1000&sdata=yDQ%2F%2BsJRHorXma34dYR8bNndmO3q0JSHqw1Gi80%2BuYA%3D&reserved=0)
[^10]:[FabLabs.io site](https://www.fablabs.io/labs/map)
[^11]:[Link to the tutorial](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-experiments/students/simone.vitale/-/blob/master/docs/project/tutorial_recycling_simone_vitale.pdf)
[^1]: These objectives are listed at the [official site of UN](https://sdgs.un.org/)
[^2]: Mikula, K., Skrzypczak, D., Izydorczyk, G., Warchoł, J., Moustakas, K., Chojnacka, K. and Witek-Krowiak, A., 2020. 3D printing filament as a second life of waste plastics—a review. Environmental Science and Pollution Research, pp.1-13.
[^3]:[Link to the documentation phase](https://fablab-ulb.gitlab.io/enseignements/2020-2021/fabzero-experiments/students/simone.vitale/project/documentation_phase/)
[^4]:[Link to the shredding process](https://fablab-ulb.gitlab.io/enseignements/2020-2021/fabzero-experiments/students/simone.vitale/project/shredding/)
[^5]:[Link to the creation of the filament](https://fablab-ulb.gitlab.io/enseignements/2020-2021/fabzero-experiments/students/simone.vitale/project/extrusion/)
[^6]:[Link to the final result of my project](https://fablab-ulb.gitlab.io/enseignements/2020-2021/fabzero-experiments/students/simone.vitale/project/printing_process/)
[^7]:[Module 3 of my learning](https://fablab-ulb.gitlab.io/enseignements/2020-2021/fabzero-experiments/students/simone.vitale/FabZero-Modules/module03/)
[^8]:[Link to the project](https://www.ulb.be/en/brussels-capital-region/co-create-value-bugs-research-project)
