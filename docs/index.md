






## About me

![ULB](ulb_fab.png)  
*Official logo of the FabLab ULB, from the official site*
[^1]  
Dear visitor, welcome to my personal Fablab page.  
Here you could find the 6 modules for the FabZero project, which is integrated in one of the ULB exams for the Master in Physics.  
My name is _Simone Vitale_ and I come from **Italy**.

![me](me.jpeg)  
*Here I am, in the beautiful countryside of Arlon*

I am passionate of nature, physics and science in general.  
Being a scientist means not only to understand science and study it, but also to try to share your knowledge and learn something from it.
This is why the FabZero experiment has attracted my attention.  
My first reason to participate was the possibility to improve my skills in informatics and numerical science.  
As a matter of fact, digital fabrication is a key ingredient for a scientist for the great possibilities that allows in term of improvement of lives of people.  
Moreover, I love nature and I would like to develop something to improve nature's condition and to limit the human's exploit of the earth.  
This is a beautiful scenery of my country, so you could understand why I am so passionate on enviornment.  
![nature](lake.jpg)
*Image of the lake of Nemi, in Nemi, province of Rome*



## My background

I was born in a nice city called Genzano di Roma and I have lived there for 20 years during my studies.
I have graduated in Rome University in September and have decided to start a new adventure here in Belgium.
I have followed the first three years of my academic career in Sapienza, one of the most important university in Italy.
There I learned the basis of physics and I then decided to move in order to make a new experience.
I decided to move to Belgium and, thanks to one of my previous exams, I discovered the Fablab environment.
I'm now in the ULB Master in physics [ULB physics Master](https://www.ulb.be/en/programme/2020-ma-phys).

For what it regards my previous projects, I have written a bachelor thesis about **Nuclear Fusion**.
Do not hesitate to contact me at **simone.vitale@ulb.be** for any information about this interesting field.
In my thesis, I described the main physical processes and the different experiments regarding the possibility to create energy for the fusion of two nuclei.



## Project

My project, which can be found in the main page of the site has been called:

#### A second life to the 3D printing filament

The following logo represents the project.

![logo](project/logo.jpg)
*Official logo of the project, designed by Christian Fuduli, an italian architect of the University of Roma3 in Rome*

Moreover I would like my project to be something useful for people in the FabLab academy and for the outer world.


## References

[^1]:[Logo FabLab ULB](https://eur01.safelinks.protection.outlook.com/?url=https%3A%2F%2Fgitlab.com%2Ffablab-ulb%2Fenseignements%2F2020-2021%2Ffabzero-experiments%2Fclass%2F-%2Ftree%2Fmaster%2Fvade-mecum%2Fimg&data=04%7C01%7Csimone.vitale%40ulb.be%7Cc4664ca3c06f4389f81408d92d7a6a9f%7C30a5145e75bd4212bb028ff9c0ea4ae9%7C0%7C0%7C637590826065413055%7CUnknown%7CTWFpbGZsb3d8eyJWIjoiMC4wLjAwMDAiLCJQIjoiV2luMzIiLCJBTiI6Ik1haWwiLCJXVCI6Mn0%3D%7C1000&sdata=yDQ%2F%2BsJRHorXma34dYR8bNndmO3q0JSHqw1Gi80%2BuYA%3D&reserved=09)
