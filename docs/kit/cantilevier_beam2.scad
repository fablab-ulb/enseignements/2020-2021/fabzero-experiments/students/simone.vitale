/*
    FILE   : cantilevier_beam2.scad
    
    AUTHOR : Simone Vitale <simone.vitale@ulb.be>
    
    DATE   : 24-02-2021
    
    LICENSE : Creative Commons 4.0 [CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/).
    
    CONTRIBUTORS : 
    - Code entirely written by Simone Vitale, basing on the following flexlink photos :https://www.thingiverse.com/thing:3016939
//*/













//parametres

rayon=3;  //rayon des cylindres qui constitue la partie qui unis un cylindre et le parallepipede
N_holes=3;  //numeros de trous pour chaque partie


// parametres pour les blocs avec les trous
height=5;  //hauteur des deux blocs
lenght=N_holes*8;//longeur des deux blocs, modifiée pour permettre d'avoir une longeur proportionné de la piece.
width=2*rayon;  //largeur des deux block



n=24 ; //distance entre les deux blocs


//module that creates a block composed by a cylinder with a cube.


module block(){
translate([n/2,0,0])
hull(){
 translate([lenght,0,0]) 
    cylinder(height,r=rayon,$fn=100);
translate([lenght/2,0,height/2])
cube([lenght,width,height],true);   
}  

}








module morceau(){
block();
rotate(180)
    block();
}


//block();
//morceau();


//definition du morceau centrale avec les trois parallepipede


  

bottom_cube_width=width;  //largeur des cubes qui doit correspondre à celle de l'autre structure

bottom_cube_height1=0.6;  //hauteur des deux cubes plus bas

bottom_cube_height2=3; //hauteur du cube plus haut


//module qui crées la structure centrale avec des formes symetrique de longeur n/3

module carres(){
union(){

  translate([-n/2+n*1/6,0,bottom_cube_height1/2])  
     cube ([n*1/3,
	bottom_cube_width,
	bottom_cube_height1],
	center=true) ;
    translate([0,0,bottom_cube_height2/2])
    cube ([n*1/3,
	bottom_cube_width,
	bottom_cube_height2],
	center=true) ;
    
    translate([n/2-1/6*n,0,bottom_cube_height1/2])
    cube ([n*1/3,
	bottom_cube_width,
	bottom_cube_height1],
	center=true) ;
 
}
}
//carres();

//ici je decide le rayon de mes cylindres
rayon_cylinders=2.5;



module  cylinders(){

for(i=[1:N_holes]){
    
    translate([n*2/3+rayon_cylinders/2+1+8*(i-1),0,height/2])
    cylinder(height+0.1, r= rayon_cylinders,center=true,$fn=100);
    
   translate([-n*2/3-rayon_cylinders/2-1-8*(i-1),0,height/2])
    cylinder(height+0.1, r= rayon_cylinders,center=true,$fn=100);
    
    }

}


//cylinders();

// distnace de 8mm entre les trous

//module totale ou on applique les elements en unissant les morceaux et en enlevant les trous.

module total(){
    union(){
        morceau();
       // block();
        carres();
        
        }
    }
translate([lenght+n/2+rayon,rayon,0]){
  difference(){
        total();
    cylinders();
  }
}



