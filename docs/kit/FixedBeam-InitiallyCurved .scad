
/*
    FILE   : FixedBeam_initiallyCurved.scad
    
    AUTHOR : Simone Vitale <simone.vitale@ulb.be>
    
    DATE   : 01-03-2021
    
    LICENSE : Creative Commons 4.0 [CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/).
    
    CONTRIBUTORS : 
    - Code based on the Paul Bryssinic code(https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-experiments/students/paul.bryssinck/-/tree/master/docs/CAD%20Files).
    It has been used to compose my kit.
    The most important modifications have been made on the parameters.
//*/






//parametres
$fn=100;
pi=3.1415926535;
thick=3.5; //epaisseur de la piece (direction z)
//fixation:
nHoles=3;
rOut=4; //largeur fixation (direction y) (la longueur de la fixation est nHoles*2*rOut)
rInn=2.5; //taille des trous
holeSep=0;//pour controler l'espacement entre les trous
//liaison
linkLen=50; //longueur de la tige depliee
linkWid=1.5;
r=2*linkLen/pi;
linkOutRad=r+linkWid/2;
linkInnRad=r-linkWid/2;

//coeur de la fixation :
module filledClip(xPos,yPos,zPos){
    hull(){
        cylinder(h=thick, r=rOut);
        translate([xPos,yPos,zPos])cylinder(h=thick, r=rOut);
        }
}


//liaison :


module link(xPos,yPos,zPos){
    difference(){
        translate([xPos,yPos,zPos])cylinder(h=thick, r=linkOutRad);
        translate([xPos,yPos,zPos])cylinder(h=thick, r=linkInnRad);
        }
}




//Assemblage et perçage des différentes parties
module assembly(){
    union(){
        //fixation 1 (touchant l'axe y)
        translate([-rOut,-r,0])rotate([0,0,180])
        difference(){
            filledClip(2*rOut*(nHoles-1)+holeSep*(nHoles-1),0,0);
            for(i=[1:nHoles]){
                translate([(2*rOut+holeSep)*(i-1),0,0])cylinder(h=thick,r=rInn);
            }
        }
        //fixation 2 (touchant l'axe x)
        translate([+r,rOut,0])rotate([0,0,90])
        difference(){
            filledClip(2*rOut*(nHoles-1)+holeSep*(nHoles-1),0,0);
            for(i=[1:nHoles]){
                translate([(2*rOut+holeSep)*(i-1),0,0])cylinder(h=thick,r=rInn);
            }
        }
        
        //on cree la liaison : un anneau dont on retire les 3/4 :
        difference(){ 
            link(0,0,0);
            union(){
                translate([-linkOutRad/2-(rOut-rInn),0,0])cube([linkOutRad,2*linkOutRad,2*thick],             center=true);
                translate([linkOutRad/2,linkOutRad/2+(rOut-rInn),0])cube([linkOutRad+2*(rOut-rInn),linkOutRad,2*thick],             center=true);
            }
        }
        }
}

//deplacer la piece pour que la tige soit centree en l'origine
assembly();

    