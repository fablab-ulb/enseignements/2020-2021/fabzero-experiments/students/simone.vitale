


/*
    FILE   : FixedBeam_initiallyCurved.scad
    
    AUTHOR : Simone Vitale <simone.vitale@ulb.be>
    
    DATE   : 01-03-2021
    
    LICENSE : Creative Commons 4.0 [CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/).
    
    CONTRIBUTORS : 
    - Code adapted from the [FLoriane Weyer  repository](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-experiments/students/floriane.weyer/) in order to compose my kit.
    *The most important modifications have been made on the parameters and some code solving a problem on the two bars that crossed.
    They were not separated but, instead, the should be separated structures.
    * An error on the parameter True has been corrected in true when needed.
    * it has been added $fn=100 in order to improve the holes.
//*/







// Paramètres

$fn=100;
// Hauteur de la pièce
height = 6;

// Nombre de trous
N_holes = 4;
// Rayon interne des trous
radius = 2.5;
// Distance centre à centre entre deux trous
distance = 8;
// Distance entre l'extérieur du trou et l'extérieur de la pièce
edge_thickness = 2;

//Distance selon y entre les deux têtes
head_distance = 40;

// Epaisseur de la tige
beam_thickness = 2;

// Calcul de la longueur de la tige et de l'angle
beam_length = sqrt(pow(head_distance,2)+pow((N_holes-1)*distance,2))-(2*radius);
angle = atan(head_distance/((N_holes-1)*distance));

//Définition du module head
module head(){
    difference(){
        hull(){
            cylinder(h = height, r = radius+edge_thickness, center = true);
            translate([(N_holes-1)*distance,0,0]){cylinder(h = height, r = radius+edge_thickness, center = true);}
            }
        for (i = [1:N_holes]){
            translate([(i-1)*distance,0,0]){
                cylinder(h = height, r = radius, center = true);
                }
            }
        }
    }
// Création de la première tête
head();

// Création de la deuxième tête
translate([0,head_distance,0]){
    head();
    }    

// Création de la première tige entre les deux têtes   
rotate([0,0,angle])   
translate([radius,-beam_thickness/2,0])
cube([beam_length,beam_thickness,height/3], center = false);

// Création de la deuxième tige entre les deux têtes   
translate([0,head_distance,0])
rotate([0,0,-angle])   
translate([radius,-beam_thickness/2,-height/2])
cube([beam_length,beam_thickness,height/3], center = false);
