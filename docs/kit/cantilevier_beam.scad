/*
    FILE   : cantilevier_beam.scad
    
    AUTHOR : Simone Vitale <simone.vitale@ulb.be>
    
    DATE   : 24-02-2021
    
    LICENSE : Creative Commons 4.0 [CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/).
    
    CONTRIBUTORS : 
    - Code entirely written by Simone Vitale, basing on the following flexlink photos :https://www.thingiverse.com/thing:3016939
//*/






//parametres

rayon=6;  //rayon des cylindres
N_holes=2;  //numeros de trous pour chaque partie


height=10;  //hauteur des deux blocs
lenght=20;//longeur des deux blocs
width=2*rayon;  //largeur des deux block



n=24 ; //distance entre les deux blocs


//module that creates a block composed by a cylinder with a cube.


module block(){
translate([n/2,0,0])
hull(){
 translate([lenght,0,0]) 
    cylinder(height,r=rayon,$fn=100);
translate([lenght/2,0,height/2])
cube([lenght,width,height],true);   
}  

}








module morceau(){
block();
rotate(180)
    block();
}


//block();
//morceau();


//definition du morceau centrale avec les trois parallepipede


  

bottom_cube_width=width;  //largeur des cubes qui doit correspondre à celle de l'autre structure

bottom_cube_height1=2;  //hauteur des deux cubes plus bas

bottom_cube_height2=5; //hauteur du cube plus haut


//module qui crées la structure centrale avec des formes symetrique de longeur n/3

module carres(){
union(){

  translate([-n/2+n*1/6,0,bottom_cube_height1/2])  
     cube ([n*1/3,
	bottom_cube_width,
	bottom_cube_height1],
	center=true) ;
    translate([0,0,bottom_cube_height2/2])
    cube ([n*1/3,
	bottom_cube_width,
	bottom_cube_height2],
	center=true) ;
    
    translate([n/2-1/6*n,0,bottom_cube_height1/2])
    cube ([n*1/3,
	bottom_cube_width,
	bottom_cube_height1],
	center=true) ;
 
}
}
//carres();

//ici je decide le rayon de mes cylindres
rayon_cylinders=2;

// valeur pour distancier les trous
b=0.1;

module  cylinders(){

for(i=[1:N_holes]){
    
    translate([n*2/3+rayon_cylinders/2+(i-1)*lenght/N_holes+b*i,0,height/2])
    cylinder(height+0.1, r= rayon_cylinders,center=true,$fn=100);
    
   translate([-(n*2/3+rayon_cylinders/2+(i-1)*lenght/N_holes+b*i),0,height/2])
     cylinder(height+0.1, r= rayon_cylinders,center=true,$fn=100);
    
    }

}


//cylinders();



//module totale ou on applique les elements en unissant les morceaux et en enlevant les trous.

module total(){
    union(){
        morceau();
       // block();
        carres();
        
        }
    }
translate([lenght+n/2+rayon,rayon,0]){
  difference(){
        total();
    cylinders();
  }
}



