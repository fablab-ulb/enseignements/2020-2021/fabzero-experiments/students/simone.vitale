# 2. Numerical conception of 3D images



## Week resume



  **Objectives**:

  * Learn about the conception of the 3D images, which are the prototypes for the 3D printer.
      * Learn the OpenScad syntax and read about Freecad syntax.
      * Improve my documentation.
      * Build a 3D model in OpenScad.
  * Learn more about the  Markdown syntax.
  * Learn about the  Licenses on codes.



## 2.1 Softwares for 3D conception. FreeCad and Openscad


### 2.1.1 The installation process


The first part of my week has been dedicated to the installation of the two programs, **Freecad** and **Openscad**.
In particular they could be integrated in order to use the best of their functions.  
Although, I have not still understood how to import files from Opencad to Freecad.
First, I have installed the two programs via the following lines:  
```
sudo apt-get install freecad  
sudo apt-get install openscad
```

For what it regards OpenSCAD, I have followed the tutorial during our course, which is in the [Fabzero tutorial](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero/computer-aided-design/-/blob/master/OpenSCAD.md).

I have had problems on the connection between OpenSCAD and FreeCad, so I have decided to pass on Freecad daily(version 0.19) in order to try it.  
The installation can be made as it follows :

* The following command permits to install the daily repository which is updated from users with new functions.
Although, the program should be considered as less stable.

```
sudo add-apt-repository ppa:freecad-maintainers/freecad-daily
```

* I then used the following command in order to install FreeCad:



```
sudo apt-get update
sudo apt-get upgrade
sudo apt-get install freecad freecad-doc

```

I have been based on [the following source](https://forum.freecadweb.org/viewtopic.php?t=995) for the installation process.




### 2.1.2 Advice on the software

The two software are really useful and can be combined together.  
**Freecad** is a software where the design can be made step-by-step building your blocks.  
**Openscad**, on the contrary, is strongly based on coding and, being a physician, I have decided to use it.  
The bulding blocks for an Openscad code can be found at [the official sheet](https://openscad.org/cheatsheet/index.html).
These are the best advice i could give to a new learner:

* Try to be modular in order to construct a logic and coherent code. In fact, it should remain comprehensible not only for you, but for the others.
* Get used to parameterize your code, as I will show later on in my code in order to permit a future modification of the parameters depending on the situation.
* Comment your code, using the signs ``` //  "explication" ```, in order to explain and clarify the construction.
* When coding with a new language, it is really common to make mistakes and lose time for ";" signs or some confusion with other codes.  Therefore, in order to solve the different errors, I could advise to comment part of the code via the command    ``` /*   */``` at the start and the end in order to identify the lines which give problems.


In order to test the skills on the 3D modeling, I have chosen a flexlink to build.
The flexlink is [the FlexLinks: Cantilever Beam Adjust](https://www.thingiverse.com/thing:3016939).  
The main problems have been the difficulty to understand the sintax of the code and the identification of the most important functions.
They are listed in the [OpenSCAD cheat sheet](https://www.openscad.org/cheatsheet/) and some examples are correlated for better understanding.

## 2.2 The project of a Flexlink

In order to test Openscad and to improve our knowledge on the sintax, we have been assigned to build the design for a 3D flexlink inspiring from [the following site](https://www.thingiverse.com/search?q=flexlinks&type=things&sort=relevant).

### 2.2.1 What is a FlexLink

A **Flexlink** is a printed structure that can be integrated with Lego in order to create a compliant mechanism.  
The notion of compliant mechanism involves the consideration of forces distribution and it has been part of [my module 3](https://fablab-ulb.gitlab.io/enseignements/2020-2021/fabzero-experiments/students/simone.vitale/FabZero-Modules/module03/).  
The development of a flexlinks has allowed me to experiment the limits of the 3D printer and to understand the interest of these structures.  
The fact that the flexlinks are Lego compatible involves the fact to conceive them in the advance with corresponding parameters.
For example, the radius of the holes and the distances between blocks.

### 2.2.2 The design of the Cantilever Beam Adjust

In order to build my flexlink, which is proposed by the following model:
![this screenshot](../images/module2/cantilever.png)

Here I add the right version of the code, where the different modules I will describe are present:
```

 /*
     FILE   : cantilevier_beam2.scad

     AUTHOR : Simone Vitale <simone.vitale@ulb.be>

     DATE   : 24-02-2021

     LICENSE : Creative Commons 4.0 [CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/).

     CONTRIBUTORS :
     - Code entirely written by Simone Vitale, basing on the following flexlink photos :https://www.thingiverse.com/thing:3016939
 //*/


//parametres

 rayon=3;  //rayon des cylindres qui constitue la partie qui unis un cylindre et le parallepipede
 N_holes=3;  //numeros de trous pour chaque partie


 // parametres pour les blocs avec les trous
 height=5;  //hauteur des deux blocs
 lenght=N_holes*8;//longeur des deux blocs, modifiée pour permettre d'avoir une longeur proportionné de la piece.
 width=2*rayon;  //largeur des deux block



 n=24 ; //distance entre les deux blocs


 //module that creates a block composed by a cylinder with a cube.


 module block(){
 translate([n/2,0,0])
 hull(){
  translate([lenght,0,0])
     cylinder(height,r=rayon,$fn=100);
 translate([lenght/2,0,height/2])
 cube([lenght,width,height],true);   
 }  

 }








 module morceau(){
 block();
 rotate(180)
     block();
 }


 //block();
 //morceau();


 //definition du morceau centrale avec les trois parallepipede




 bottom_cube_width=width;  //largeur des cubes qui doit correspondre à celle de l'autre structure

 bottom_cube_height1=0.6;  //hauteur des deux cubes plus bas

 bottom_cube_height2=3; //hauteur du cube plus haut


 //module qui crées la structure centrale avec des formes symetrique de longeur n/3

 module carres(){
 union(){

   translate([-n/2+n*1/6,0,bottom_cube_height1/2])  
      cube ([n*1/3,
 	bottom_cube_width,
 	bottom_cube_height1],
 	center=true) ;
     translate([0,0,bottom_cube_height2/2])
     cube ([n*1/3,
 	bottom_cube_width,
 	bottom_cube_height2],
 	center=true) ;

     translate([n/2-1/6*n,0,bottom_cube_height1/2])
     cube ([n*1/3,
 	bottom_cube_width,
 	bottom_cube_height1],
 	center=true) ;

 }
 }
 //carres();

 //ici je decide le rayon de mes cylindres
 rayon_cylinders=2.5;



 module  cylinders(){

 for(i=[1:N_holes]){

     translate([n*2/3+rayon_cylinders/2+1+8*(i-1),0,height/2])
     cylinder(height+0.1, r= rayon_cylinders,center=true,$fn=100);

    translate([-n*2/3-rayon_cylinders/2-1-8*(i-1),0,height/2])
     cylinder(height+0.1, r= rayon_cylinders,center=true,$fn=100);

     }

 }


 //cylinders();

 // distance de 8mm entre les trous

 //module totale ou on applique les elements en unissant les morceaux et en enlevant les trous.

 module total(){
     union(){
         morceau();
        // block();
         carres();

         }
     }
 translate([lenght+n/2+rayon,rayon,0]){
   difference(){
         total();
     cylinders();
   }
 }






```

**Design step by step**

I have tried first to learn how to draw a cube, using the command:

```
   cube([lenght,width,height]       
```
and then I have translated it in order to create a symmetry on the z axis.

Then, I have applied the **Hull** command in order to merge it with a cylinder of parameterized rayon.

![this screenshot](../images/module2/bloc.png)

The module **morceau** replicates the block in order to obtain the two external pieces.
![this screenshot](../images/module2/2.png)
Now the central structure with the three cubes should be conceived.  
I proceeded in the creation of the three cubes at the middle paying attention to the following characteristics:  

* The structure must be symmetrical.
* The three cubes are structured with two high which are the same and one which is different.

![this screenshot](../images/module2/cubes.png)

Then I have constructed the holes which are applied on the two blocks.  
It should be noticed that the parameter \( N_{holes} \) permits to change the number of holes.  
Finally, I have translated the module2/system in order to easily measure the distances.

The final result is the following:

![](../images/module2/final.png)

The code has been organized in order to be as free as possible from the choice of parameters.
This was my first conception of the code: [my code](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-experiments/students/simone.vitale/-/blob/master/docs/kit/cantilevier_beam.scad).  
Some problems have been solved regarding the distribution of the holes and the **right code** is [here](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-experiments/students/simone.vitale/-/blob/master/docs/kit/cantilevier_beam2.scad).



I have not yet approached Freecad because it was not necessary for my design.
Freecad can easily imports Openscad files under the form **.stl**.  
Instead, if you try to import files **.cad**, they are refused.  



## 2.3 The LEGO compatibility

All the information on LEGO compatibility have been found on [the following site](https://www.tipsandbricks.co.uk/).
![](../images/module2/dimension_lego.jpg)
The image shows the dimensions that have to be respected when speaking about Lego compatible FlexLinks.  
I have discovered it after my first experience with [the 3D printer](https://fablab-ulb.gitlab.io/enseignements/2020-2021/fabzero-experiments/students/simone.vitale/FabZero-Modules/module03/).  
Therefore, the rayon of the holes should be about 5 mm in order to consider also the sensibility of the printer(0.2 mm).

**Advice**: when designing the cylinders for the holes and the borders of the Lego compatible pieces, pay attention to start your code with "$fn=100" or more.  
It is really important to have high enough number of faces that approximate the curve so that the holes are as smooth as possible.

Another important note on the dimension of the holes is that, after the printing process, one side of the flexlink has a different diameter of the hole.  
It is strongly due to the fact that one side touches the base of the printer and the other side of the hole is printed on the air.
This notion particular applies on the flex material.
### Little spoiler: My first essay on the printer was not good..

![my print](../images/module2/printing1.jpeg)
Really unsatisfactory because it was not really flexible.
The problem was in the length and the height of the central cubes.
Here I put [the right code](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-experiments/students/simone.vitale/-/tree/master/docs/kit/cantilevier_beam2.scad) that gives the more flexible form.





## 2.4 Licenses applied on codes

[The MIT site](https://integrity.mit.edu/handbook/writing-code) has been the reference on how to manage a code from another person and how can I use it citing the source.  
When we speak about Licenses that could be applied a common one is **The Common creator license**, which is the one I applied on my code( Creative Commons 4.0 [CC BY-NC-SA 4.0]).  
In particular [the official site](https://creativecommons.org/licenses/by-nc-sa/4.0/) resumes the different treatment of the code that could be made.
With this license you are free to:

* Share — copy and redistribute the material in any medium or format
* Adapt — remix, transform, and build upon the material.  


Another important and common license is the **Mit license**.  
More information can be found [here](https://en.wikipedia.org/wiki/MIT_License).  
However it imposes less limits than the creative commons, in particular you can "use, copy, modify, merge, publish, distribute, sub-license, and/or sell copies of the Software".

It is **advisable** to use the same license in every program or code of the same project in order to facilitate the citation of your files.  
For that reason, we have been advised to create a file license to upload in the main folder.


## Useful links

- [OpenSCAD](https://www.openscad.org/cheatsheet/)
