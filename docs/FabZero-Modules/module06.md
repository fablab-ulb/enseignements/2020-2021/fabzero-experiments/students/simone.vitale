# 6. Electronics 2 - Fabrication


## Week resume

**Objectives**:

* I have made a development board through a Printed Circuit Board.
* I have tested the board with simple programs.
* Demonstrate correct workflows and identify areas for improvement if required.
* I have learned how to solder.
* I have described the process of stuffing, de-bugging and programming.  




## 6.1 My own Board
This week I have been involved in the construction of my own board.  
The fifth week we had programmed on an Arduino UNO board, which is equipped with 8bits microprocessor without USB, called [AtMega328P](https://www.microchip.com/wwwproducts/en/ATMEGA328P).  
In order to build my board, the [Atmel SAMD11C](https://www.microchip.com/wwwproducts/en/ATSAMD11C14) has been used instead, which is a 32bits microprocessor integrating USB natively.  
The AtMega328P has however a bigger size for programming than the board we are going to construct.  
It has been decided to build a simple card in order to test our capacity in soldering.  
However, it should be observed as the pieces we fixed were really difficult to solder due to their size.  
In analogy with Arduino UNO, the bootloader had been already uploaded on our processor, which is a mechanism to communicate with the USB port.

### 6.1.2 The Board construction
In order to build the board I have followed the [tutorial](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero/electronics/-/blob/dd760ea3b49b74d278b1eec1e4be9e5a45a986a6/Make-Your-Own-Arduino.md) on the Fablab site.
The following material has been employed:

* One [Atmel SAMD11C](https://www.microchip.com/wwwproducts/en/ATSAMD11C14) as core
* The [bootloader](https://github.com/mattairtech/ArduinoCore-samd/blob/master/bootloaders/zero/binaries/sam_ba_Generic_D11C14A_SAMD11C14A.bin), which was already programmed into the chip to ease your work.
* A regulator that converts the 5V from the USB port in 3,3V.
* A capacitor in order to make the voltage stable.
* The Grey material, which is stuck to the board in order to allow its entrance in the USB port.

![material](../images/module6/material.JPEG)

I have then followed the steps which were proposed in order to solder the different pieces.  
### 6.1.3 Step 1: analysis of the board
**The First thing to do** has been the analysis of the pre-milled board.
![board](../images/module6/premilledboard.JPEG)
It has been checked that the board, in copper, had its lines well traced.
As a matter of fact, each lines represents an electric connection and eventually could be reinforced with metal around the inferior part because, entering in the USB port, it can cause the damaging of the lines.

### 6.1.4 Step 2:the welder device
There were two welders available:

* The first one, which is presented in figure, has been used and allows to set the exact temperature.
I have generally solder around 300°.
![solder1](../images/module6/traditional_solder.JPEG)
* The gas solder has been used by other members of the Fablab and does not allow to set the temperature.
![solder1](../images/module6/gas_solder.JPEG)

I have then learned how to use it.  
In particular it must be payed attention that the temperature of the welder could join 400°, which requires particular caution.  
**Errors with the welder:** during my soldering experience of the day I have been confronted on some errors that I would like to share.

* For example, I had a tendence to put **too many material on the board**, material which is directly obtained melting a wire.
* Another error was in the tip of the welder.  
In particular I have sometimes forgotten to remove the oxidation on it which is given by its utilization.  
Therefore, do not forget to cover the tip of the welder with the melted metal and then pass it on the sponge which is annexed to the device.
* The tip of the welder should always be smooth and shiny.
The results of this imprecision could be easily seen when looking on the different photos.
### 6.1.5 Step 3: the soldering of the microprocessor
After having practiced a little bit with the soldering, I have started from the soldering of the first and fundamental element, the **microprocessor**.  
The proceeding has been the following:

* Sold the first path and pay attention to align it well to the wire.
![solder1](../images/module6/first_path_microprocessor.JPEG)
The four path on it will be fundamental, so take attention not to break the different paths of the circuit.  
The **Atmel** must be placed with the white circle to the left-side with reference to the precedent photo.  
In this case you should read the Atmel description on the microprocessor in the right way when rotating the USB of 90° to the right.
* At this moment you will still have some degree freedom, so pay attention to rightly orient your microprocessor.
At this moment I soldered the path diagonally opposed.
![solder2](../images/module6/Error_onthepath.JPEG)
In figure I made an **error** because I put too many material and it has connected two path, which could cause a short circuit.
* Solder the regulator as I made on figure, which reduces the tension.
Before doing it, I have soldered the other paths and I have cleaned part of the board.
![solder3](../images/module6/regulator.JPEG)
* Finally place the capacitor on the board.
It has been a really difficult part due to the size of the piece.
![solder4](../images/module6/complete_board.JPEG)
* Finally stick the USB connector, which gives some width to the board and allows to insert it in the USB port.
![solder5](../images/module6/usb_connector.jpeg)

During the different proceeding I have been attentive not to stay too much time with the hot tip on the paths because it could break the microprocessor which can usually support up to 80°.

### 6.1.6 The test of the Board
In order to test if the board had been correctly settled I have been inserted in the USB connector of my PC.  
As the tutorial stated, I have had the following result writing in the terminal the command:

```
dmesg -c -w
```
The result I obtained was the following:
![solder5](../images/module6/verification_usb.png)
This confirmed that the board had been functioning.  
Therefore, the board could be considered as an equivalent of Arduino UNO.
Even though, less memory is disposable for programming on it.

After having verified that the board was working, I should have soldered a led on the pin number 5, which corresponds to the Arduino pin 15.  
Here I show my final result.  
![solder5](../images/module6/temporary.png)

### 6.1.7 Improvements to apply
The result was clearly not as expected, because I made the following errors:

* I put too much metal on the resistance and the Led, risking some short circuit, and impeaching their functions.
* Moreover, I also tried to remove the resistance, which did not connect well and I removed part of the board wire.
I then stopped to test the card because it could be dangerous for my USB port.
Nevertheless, I decided to try to save the board and improve its functions.  
Therefore, I decided to:

* Remove the dangerous structure I created on the left side.
* Soldering better the following pieces using a cleaner tip, in the following figure I want to underline that all the tip should have the same brilliant color of the middle.
![tip](../images/module6/tip.jpeg)
* I tried to continue my circuit installing the LED and the resistance on the right part which allow to look when the board is functioning.

**Pay attention**: The conductive parts are made of copper, and can be thought as wire.   Therefore it is important how to connect them, avoiding the connection of two wires.

## 6.2 The improvement of the board

### 6.2.1 The cleaning of the structure

In order to clean a little bit the mass of metal on the left, I used the pump which allows to remove the material after having heated it.
![compressor](../images/module6/compressor.jpeg)
The result on the left part has been satisfactory:
![step](../images/module6/step_board.JPEG)

### 6.2.2 The soldering process of the right part

Having failed with the left part, I decided to install the resistance and the LED in the right part.  
**Advice on the LED**: in order to turn on the LED, it is necessary that it is placed in the good sense.  
For this, we need to use the multimeter and test, with the LED symbol, the side where the LED turns on.
Usually, the black wire of the multimeter is connected to the ground.
![step](../images/module6/led.JPEG)
The black side should correspond to the higher voltage.  
Apparently, the result was good enough as I show here.
![step](../images/module6/finalboard.JPEG)

Nevertheless, its insertion in the USB port and its utilization for Arduino do not turn on the LED.  
It can be due to the copper wire which is damaged in my board or an error in the connections.

## 6.3 Test of the board, Arduino configuration

After having modified my board as explained in the last section, I dedicated to test my board.
The first test has been to check if it still functioned.  
Therefore, I applied the command as described before and obtained the result:
![verification](../images/module6/device2.png)
Luckily it is still working.

### 6.3.1 Configuration in the Arduino IDE

I applied the following passages to configure Arduino to use the SAMD11C for the Arduino IDE:

* As a first step, I added the following url to the **Addtional board url manager** in the preferences.
```
https://www.mattairtech.com/software/arduino/package_MattairTech_index.json
```
This allows to introduce an external board manager, which can be then installed.
* I went then to the board manager in the Tools section and installed the first manager, found searching **mattair**.
* To conclude the configuration I selected the following parameters from the Menu, in the Tools section:
  * Board : MattairTech and then Generic D11C14A.
  * Clock : INTERNAL_USB_CALIBRATED_OSCILLATOR.
  * USB-CONFIG : CDC_ONLY.
  * SERIAL_CONFIG : ONE_UART_ONE_WIRE_NO_SPI.
  * BOOTLOADER : 4kB Bootloader.
  * Payed attention to select the right port of the PC.
I left the other parameters as they were.

When using the board, it is necessary to create a table of correspondence between the Arduino pins and our board pins.
Here it is a table with the correspondence, which should be consulted before programming in the IDE:
![pinout](../images/module6/pinout.png)


### 6.3.2 First test, the LED

The tutorial proposed to use a program in order to turn on the LED on the 5th position.  
Nevertheless, I have not been able to place it.  
For completeness, I have tried to compile the code **blink.ino** that has also been added to the repository.

The following program, to turn on the LED that has not been possible to install in the left part, is contained [in my Arduino repository](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-experiments/students/simone.vitale/-/blob/master/docs/Arduino/blink/blink.ino).
```
/*
  Title Blink.ino
  author: Simone Vitale
  Date: 20/04/2021
  License: Mit license
  Turns an LED on for one second, then off for one second, repeatedly.
  This example code can be found in http://www.arduino.cc/en/Tutorial/Blink

*/

// the setup function runs once when you press reset or power the board
void setup() {
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(15, OUTPUT);
  //enable to write in the serial
  SerialUSB.begin(0);
}

// the loop function runs over and over again forever
void loop() {
  digitalWrite(15, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(1000);                       // wait for a second
  digitalWrite(15, LOW);    // turn the LED off by making the voltage LOW
  delay(1000);// wait for a second
  SerialUSB.println("The card can compile the code");

}

```
However, I clearly can not get any actual result from it because the LED had not been placed.  
I decided to take a video to show that the file compiled well and it could have functioned.

<video controls muted>
<source src="../../videos/board/blink.mp4" type="video/mp4">
</video>

Clearly, the **only use of my board could be as a compiler**.

### 6.3.3 An example of practical use as a compiler
Finally, in order to show a more complete example and test its memory, I decided to compile a more complex code.  
It computes the factorial of a given number and print it on the serial.  
In order to print on the serial the command "SerialUSB.begin(9600);" must be inserted in the loop and "SerialUSB.println(result);" allows to print.

The following program, to compute the factorial of an integer, is contained [in my Arduino repository](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-experiments/students/simone.vitale/-/blob/master/docs/Arduino/factorial/factorial.ino).

```

/*
  Title Factorial.ino
  Author: Simone Vitale
  Date: 20/04/2021
  License: Mit license
This code simply computes the factorial of the number factNum and print in in the serial.

Code issued from:  
https://github.com/BraveTea/Factorial-in-Arduino/blob/master/Factorial__borrowed_code_.ino
and modified in order to suit the Generic D11C14A board manager.
*/




int factNum = 4;

void setup()
{
  SerialUSB.begin(9600);
}

void loop()
{
  int result;
  result = factorial(factNum);
  SerialUSB.println(result);
  delay(5000);

}

int factorial(int n)
{
  if (n == 1)
  {
    return 1;
  }
  else
  {
    return n * factorial(n-1);
  }
}

```

I decided to take a video to show that the file compiled well and that the result is correctly printed.

<video controls muted>
<source src="../../videos/board/factorial.mp4" type="video/mp4">
</video>

It should be noticed another receiving result, the LED placed on the right part does not turn on as expected.

## 6.4 Conclusions

It was the first time I soldered and I have found some difficulties in doing it.  
The main difficulty has been that the pieces were really small and it was not easy to solder them correctly.
Finally, I hope that my errors should help you if you want to reproduce the board avoiding them.

Better results have been, luckily, achieved by some of my colleagues.
For example, the page of Doriane, which can be easily accessed from [here](https://fablab-ulb.gitlab.io/enseignements/2020-2021/fabzero-experiments/students/doriane.galbez/FabZero-Modules/module06/), is a really good example.
