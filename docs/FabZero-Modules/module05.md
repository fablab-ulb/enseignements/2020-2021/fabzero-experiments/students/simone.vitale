# 5 Electronics 1 - Prototyping

## Week resume

**Objectives**:

* Learn more about the structure of Arduino and its additional components.
* Learn about the different uses that could be made of Arduino.
* Build a circuit which sorts out an output and measures something.
* Write an Arduino commented code, improving my knowledge on the syntax.
* Make a video where my board executes the code required.  

## 5.1 Arduino
The first part of the week has been dedicated to learn about **Arduino** and its components.
It is useful to precise that when speaking about Arduino we could mean:

* The **Arduino IDE**, which is a compiler where a c-code can be written and it is then converted in Arduino language.
To install the Arduino IDE it is possible to go to the [official page of Arduino for linux installation](https://www.arduino.cc/en/guide/linux).  
The language, which is used, is explained in the [tutorial page](https://www.arduino.cc/en/Guide).
However, Arduino could also be used through external libraries which practically allow to do everything.  
* The Arduino **hardware**, which is showed in the following image(Arduino UNO model).
![Arduino image](../images/module5/Arduinouno.png)
Important attention should be made on the input tension, so make an **ordinate circuit** in order to avoid _short circuit_.
The maximum input voltage is 5V, however I will show how to add external alimentation.

### 5.1.2 The Arduino board
An important division, which is applied on the Arduino board is between analogical and digital signal.  
The two signal can be easily converted via some Arduino libraries.
Let's see the differences of the two:

* The **analogic signal** is emitted from 0 to 5V, or 3.3V, depending on the micro-controller(the Arduino brain).  
Its ports(pins) are indicated as A#number.
* The digital signal is 0 or 1 without shades.  
However, some RC circuits could make a mean on the digital signal in order to give intermediate results.
This is the case of the PWM digital pins.  
An example is given by the digital pin 6 which, at high frequencies, creates a pseudo-analogical signal.

We can resume the security limits to respect on the board:

* Principal alimentation of 5V.
* Exiting current of maximum 40 mA per pin.
* Maximum current of 200mA for Arduino, which means that each group(digital and analogical) could not pass over 100 mA.
* The alimentation could be given by an USB. It is advisable to stop the Arduino alimentation when working on a circuit, do not forget that human body is a conductor.

### 5.1.3 Arduino modules
Arduino can be equipped with different modules on the shield, which include sensors, magnetic field generators and others of different sort.  
The list of the main components has been presented in the following [page, from the FabLab](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero/electronics/-/blob/56a8960118866d055633035714c50a5ee2434985/IO-Modules.md).
However, more information can be easily found on the site of the producer.  
**Pay attention** to the alimentation that each module requires in order to allow the functioning of the component.


### 5.1.4 Examples of Arduino utilization and codes
I decided to treat in this module some examples of Arduino codes, which I composed starting from some examples on internet.  
As a physicist I have already, during an high-school project, designed a weather-station with the help of Raspeberry.  
Therefore, I have already used this kind of technologies on a simple LED system.
This is the reason why I will not start from a one piece example as a LED, but with other kind of sensors and buttons in order to improve my knowledge.  
Here I propose 4 examples of coding.

## 5.2 Examples on the board

### 5.2.1 Example 1: the use of the board without programming
Let's imagine that you hold an Arduino but, unluckily, in that moment can not program.  
The Arduino board can be easily used without coding, which could be useful.
Here I show a simple example elaborated with Ellias in which I have tested the following modules:

* A button that open and close a circuit(on the right).
* A buzzer that emits sounds(on the left).

![buzzer and sensor](../images/module5/buzzer_and_button.JPEG)

They have been applied on the breadboard as it follows:

![buzzer and sensor](../images/module5/board_buzzer_button.JPEG)

The logic of the circuit is really simple, the pushing of the button opens the circuit.  
When the circuit opens, current can pass through the circuits up to the buzzer which finally emits a sound.  
Notice the following advice on the breadboard(which is the white structure where I apply my modules):

* The breadboard could be divided in 4 sections:
    * The first one from the left where the vertical lines will have the same voltage.
    * The left-central, which is not connected to the right-central, where horizontal lines have the same voltage.
    * The right-central, which has the horizontal lines at the same tension.
    * the right part(two columns) which could be exploited to take 5V from the same vertical line.
* It is fundamental to put a resistance in order to reduce the current on the circuit.
We can see that in a particular case the resistance could be omitted, because the pin **13** has an internal resistance.
* In this case we have just used the voltage provided by the Arduino pins 5V and ground in order to build the circuit.

Remember that \(  V=\sum V_i=RI \), where the resistance is measured in **Ohm**.
The value of the resistance is not so important in this case due to its role.  
The voltmeter allows to measure the tensions and the resistances when designing a circuit.
Examples on the use of a voltmeter can easily be found on the web.
We could still deepen this circuit but, for the moment, let's try to go beyond.  
The example 2 allows to enter in the real applications of Arduino.  
In fact, the sensors in Arduino are a fundamental feature to build and design objects and to measure physical values.

### 5.2.2 Example 2: the CAPACITIVE TOUCH SENSOR SWITCH(VMA305)
The first step of the analysis has been to consult the user manual.  
The manual of the sensor can be found at [the official vendor site](https://www.velleman.eu/products/view/?id=435524) in the section **user manual**.  
The following code has been inspired and adapted from the open-source Arduino codes and can be found [here](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-experiments/students/simone.vitale/-/blob/master/docs/Arduino/capteur_capacitif/capteur_capacitif.ino).

```
/*

   Title: capteur_capacitif.ino
 Authors: Simone Vitale and Ellias Ouberri
 Date: 18/03/2021
 Licence: MIT license.
 Contributors: the code has been issued form the Arduino examples on the official site http://www.arduino.cc/en/Tutorial.
Code on the capactive captor(wma305)




*/

int analogPin = A0; // here we declare that an analog pin, the 0 , should be called analogPin, a variable

int val = 0;  // variable to store the value read

// function to declare the setup
void setup() {
  Serial.begin(9600);           //  setup serial: opens serial port, sets data rate to 9600 bps
}

//cycle on what the programme will do
void loop() {
  val = analogRead(analogPin);  // read the input pin
  Serial.println(val);          // debug value and print on the serial which could be accesed in Tools/serialplotter
}


```


The circuit has been made as it follows:

![](../images/module5/capacitive_circuit.JPEG)

The capacitive sensor has been installed following the procedure on the user manual, which is [in the following page](https://www.velleman.eu/downloads/29/vma305_a4v01.pdf).  
More information on the VMA305 can be found to the [vendor site](https://www.velleman.eu/products/view/?id=435524).  
The sensor emits a signal when something touches its surface.  
We have then tested it verifying the variation on the analog signal he furnished to the Arduino serial, where the value of the output pin is printed and plotted.  
The serial can be accessed through the Arduino menu in the section:


```

Tools/Serial monitor or Serial plotter
```

The test the sensor can be made with the code via the Arduino IDE.  
It is easily made through the left top commands or the Sketch menu.
After the compilation the sensor did not read any signal as expected and, when applying some force on its surface, the analogical tension varied.
The variation in analogical tension is plotted in the serial in function of time:
![](../images/module5/arduino_example_of_sensor.png)

In general the signal in Arduino Uno will be showed as an integer number from 0 to 1023.
The conversion in tension can easily be achieved by the following formula:

\[  V_{out}=V_{in} \cdot \frac{N}{1023}  \]  


### 5.2.3 Example 3: Hall sensor for magnetic field(VMA313) and magnetic field generator(VM431)

The third example, which we have proposed but not achieved the same day, has been the conception of a magnetic sensor.  
A brief description of the magnetic hall sensor is mandatory.  
My explication is strongly based on the explication of [this site](https://www.electronics-tutorials.ws/electromagnetism/hall-effect.html).  
Hall Effect Sensors are devices which are activated by an external magnetic field.  
The magnetic field has two main characteristics: a flux density(B) and polarity (North and South Poles).  
The output signal from a Hall effect sensor is the function of magnetic field density around the device.  
When the magnetic flux density around the sensor exceeds a certain pre-set threshold, which is 10 Gauss in our case, the sensor detects it and generates an output voltage called the Hall Voltage, \(V_H \).  
The main application for Hall Effect Sensors is for proximity sensors.  
They differ from the capacitive sensors because they do not require to touch the piece in order to detect something but can work at brief distances.  
They can be used instead of optical and light sensors were the environmental conditions consist of water, vibration, dirt or oil such as in automotive applications. Hall effect devices can also be used for current sensing.

The first, and more simpler, conception of the circuit was to build an Hall sensor and verify through the serial if the presence of magnetic field due to some magnets changed its measurement.  
The code which has been used is [here](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-experiments/students/simone.vitale/-/blob/master/docs/Arduino/hall/hall.ino).

```

/*
   Title: Hall.ino
 Authors: Simone Vitale and Ellias Ouberri
 Date: 18/03/2021
 Licence: MIT license.

 Contributors: the code has been issued form the union of different examples with some debugging on the code.
- The HALL (HOLZER) MAGNETIC SWITCH MODULE VMA313 has been programmed using the user manual:  "https://www.velleman.eu/downloads/29/vma313_a4v01.pdf"  
- The example code for the ELECTROMAGNET MODULE VMA431 has been issued from the following page(user manual):"https://www.velleman.eu/downloads/29/vma431_a4v01.pdf"


*/

int val = 0;  // variable to store the value read


/*
Turns on an Electromagnet on for one second, then off for one second, repeatedly.
This example code is in the public domain.
VMA431 connections
VCC = +5V
GND = GND
SIG = Digital line 0 (any line can be used)
*/
int Electromagnet = 0;
int LED = 13;
// the setup routine runs once when you press reset:
void setup() {
    Serial.begin(9600);           //  setup serial
// initialize the digital pin as an output.
  pinMode(Electromagnet, OUTPUT);
  pinMode(LED, OUTPUT);
}
// the loop routine runs over and over again :
void loop() {

  digitalWrite(Electromagnet, HIGH); // turn the Electromagnet on (HIGH is the voltage level)
  digitalWrite(LED, HIGH); // turn the LED on (HIGH is the voltage level)
  //delay(10000); // wait for a second
  digitalWrite(Electromagnet, LOW); // turn the Electromagnet off by making the voltage LOW
  digitalWrite(LED, LOW); // turn the LED off by making the voltage LOW
  //delay(10000); // wait for a second
  val = analogRead(analogPin);  // read the input pin
  Serial.println(val);          // debug value



}
```
The circuit has the following structure:

* A hall sensor of magnetic field, which however is really unstable due to the different magnetic fields in the room.  
The documentation on the sensor can be found at the following page on the [site of the product ](https://www.velleman.eu/products/view/?id=435540) and its manual.
* A magnetic field generator, which has been alimented from an external source than Arduino is described on [the site of the product ](https://www.velleman.eu/products/view/?id=439220).

In order to supply the Electromagnet, the following structure has been added to the breadboard.

![](../images/module5/alimentation_suppletive.JPEG)
It allows to give the 5V which are required for the magnet.


The circuit has taken the following form:
![](../images/module5/hall_magnet_circuit.JPEG)


In the first test, which has been conducted the same day, it has been plotted the measured value to the serial:

* The first figure shows the case where no magnet was placed.
![Before](../images/module5/hallcaptorwithmagnet2.png)

* The second figure shows a clear variation when the magnet is alimented.
![After](../images/module5/hall_serial.png)

Nevertheless, some considerations should be made:

* The signal was really unstable, and this did not assure the quality of the measurement.
* The sensor measures the magnetic field density, through a tension called **Hall tension**: how to convert an analogical value which will be showed how to convert in the next section.
* The circuit was however not enough satisfying because it lacked some support for the _visualisation of the result_ and the _result was not so clear regarding the effect of the magnet_.

Therefore, in autonomy, I decided to improve the circuit and to give a fourth example of the code.

### 5.2.4 Example 4: Hall sensor for magnetic field(VMA313) and magnetic field generator(VM431)+4-digit screen

The first innovation has been the introduction of the 4-digit screen where the analogical signal has been visualized in order to observe its variation.  
The screen is the following model: The 4-DIGIT DISPLAY WITH DRIVER MODULE (TM1637 DRIVER)WPI425.  
[From this page](https://www.velleman.eu/support/downloads/?code=WPI425) every information about the product can be downloaded.
Nevertheless, in order to show something on the screen, some code must be written using the library "TM1637Display.h" which should be included at the start.  
I have downloaded following the information from [the following site](https://www.makerguides.com/tm1637-arduino-tutorial).

The code, which has been used, describes the case of the presence(or not) of the magnet changing the value of the variable **magnet**:  

* If magnet=0, the simple measure of the room field is present.  
* If magnet=1. the user can also change the variable **timeofmagnet** in order to test its functioning.  
After this time, the magnet simply turn off and the signal converges to the precedent case.

Another important improvement has been the insertion of a control parameter.  
It is called \( B_{critical} \), and allows to measure the critical value of \( V_{H} \), the tension of Hall for the sensor.  
The value which is measured is in analogic and can be easily converted to digital by the formula:

\[ V_{out} = V_{in} \cdot   \frac{N}{1023}   \]

,where \(V_{in}=5V\) in our case.  
Depending on the circuit, the value of the magnetic density can be easily extracted following the formula:

\[ V_H=R_H  (\frac{I}{t} \wedge B)\]

where:

* \(V_H\) is the Hall Voltage in Volts, which is actually measured.
* \(R_H\) is the Hall Effect coefficient, which should be measured through calibrations.
* I is the current flow through the sensor in Ampères, which can be obtained knowing the value of the resistance.
* t is the thickness of the sensor in mm, which can be measured.
* B is the Magnetic Flux density in Tesla, which will be found after having calibrated the device.

These information have been obtained by the [maker guide for the screen.](https://www.makerguides.com/tm1637-arduino-tutorial/)



The code, which has been used for the simulations, is  [at the following link](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-experiments/students/simone.vitale/-/blob/master/docs/Arduino/HallEffect/HallEffect.ino).


For convenience, I have also copied it here.


```
/*
    Title: Halleffect.ino

  Author: Simone Vitale
  Date: 20/04/2021
  Licence: MIT license.

  Contributors: the code has been issued form the union of different examples with some debugging on the code.
 -The information for the display TM1637 has been issued from the site "https://www.makerguides.com/tm1637-arduino-tutorial/", from which I have taken the code for activating it and writing on it.
 - The HALL (HOLZER) MAGNETIC SWITCH MODULE VMA313 has been programmed using the user manual:  "https://www.velleman.eu/downloads/29/vma313_a4v01.pdf"  and here "https://www.electronics-lab.com/project/using-hall-effect-sensor-arduino/"
 - The example code for the ELECTROMAGNET MODULE VMA431 has been issued from the following page(user manual):"https://www.velleman.eu/downloads/29/vma431_a4v01.pdf"


*/


 // Include the libraries:

 #include <Arduino.h>    //basic library for Arduino programming
 #include <TM1637Display.h>  //LIbrary which is dedicated to the 4-digit led screen, the module is called TM1637 and is a display. The library has been suggested from this site https://www.makerguides.com/tm1637-arduino-tutorial/

 // Declaration of the pin variables

 int Electromagnet = 0;    //pin for giving the input to the electromagnet
 int hallSensorPin = 2;    //pin to receive the Hall sensor reaction  
 int ledPin =  13;    //pin, with annexed resistance in Arduino, which allows to control the LED
 int analogPin = A2; // potentiometer wiper (middle terminal) connected to analog pin 2
                     // outside leads to ground and +5V
 int val = 0;  // variable to store the value read by the sensor, which will be then printed
 int state=0;
 int B_critical = 320; // critical value for the magnetic field, going over this value will activate the LED
 int electromagnet = 4; // variable which activate the program without the electromagnet if equal to 0 and activate the electromagnet if a number different from 0
 int timeofmagnet= 1000;  //Variable which describes the time for which the magnet must be switched on in ms
 // Define the connections pins for the screen:
 #define CLK 7
 #define DIO 6


 // Create display object of type TM1637Display, feel free to change the definition of the pins depending on your circuit:
 TM1637Display display = TM1637Display(CLK, DIO);


 // Create array that turns all segments on:
 const uint8_t data[] = {0xff, 0xff, 0xff, 0xff};
 // Create array that turns all segments off:
 const uint8_t blank[] = {0x00, 0x00, 0x00, 0x00};
 // You can set the individual segments per digit to spell words or create other symbols:
 const uint8_t done[] = {
   SEG_B | SEG_C | SEG_D | SEG_E | SEG_G,           // d
   SEG_A | SEG_B | SEG_C | SEG_D | SEG_E | SEG_F,   // O
   SEG_C | SEG_E | SEG_G,                           // n
   SEG_A | SEG_D | SEG_E | SEG_F | SEG_G            // E
 };



 void setup() {

   // Clear the display from previous results:
   display.clear();
   delay(1000);
   //define the role of the pin
   pinMode(ledPin, OUTPUT);      
   pinMode(hallSensorPin, INPUT);

   Serial.begin(9600);           //  setup serial for printing the values of the magnetic field
 // initialize the digital pin as an output.
   pinMode(Electromagnet, OUTPUT);
 }

 //The code will first activate the electromagnet for 1 second,5 seconds etc.. depending on the choice of the delay time and the value of B will then measured. The code will then measure the magnetic field without the magnet.
 void loop(){

   state = digitalRead(hallSensorPin);

   // Set the brightness of the screen:
   display.setBrightness(7);


   if(electromagnet !=0) digitalWrite(Electromagnet, HIGH); // turn the Electromagnet on (HIGH is the voltage level)

   delay(timeofmagnet); // wait for a second with the Eletromagnet activated

   val = analogRead(analogPin);  // read the input pin of the Hall sensor
   Serial.println(val);          // debug value


   // cycle on the LED which allows to activate it or not depending on a critical value which can be defined at the start:
   if (val>B_critical) {        
     digitalWrite(ledPin, HIGH);  
   }
   if (val<B_critical){
     digitalWrite(ledPin, LOW);
   }

   // Print the measured value of the density of the magnetic field in gauss:
 display.showNumberDec(val);
// val2=val*5/1023   //line which allow to obtain the value of the Hall tension in Volts
 }

```

Looking at the Arduino code, is important to underline that a library should have been installed:
The library is called: "TM1637Display" and can be easily installed using the library installer of Arduino IDE:

* Go to tools/Manage libraries/ and write tm1637.
* Then choose the library by Avishay Orpaz, whose reference can be find [here](https://github.com/avishorp/TM1637).

Therefore, to conclude this module, I have tested the two scenarios of my code.
This is my final circuit.

![final circuit](../images/module5/circuit_complete.JPEG)


#### Case without electromagnet

This is the video I propose where the circuit has been tested.



<video controls muted>
<source src="../../videos/Arduino/nomagnet2.mp4" type="video/mp4">
Video without magnet
</video>


#### Case with electromagnet and different intervals of time

Finally I have tested if the electromagnet was the real responsible for the variation of the signal.  
The following two videos show that, depending on the chosen time, the led light, which switched on when the value was over the critical value of tension, switched off exactly after the switching off of the magnet.

Here I show the first case with 1 second time:

<video controls muted>
<source src="../../videos/Arduino/magnet1.mp4" type="video/mp4">
Video without magnet
</video>

Here It is the case of 5s.

<video controls muted>
<source src="../../videos/Arduino/magnet5.mp4" type="video/mp4">
Video without magnet
</video>

I also add a screenshot in order to observe the variation on the serial.  
It must be noticed that in this case the oscillating form could suggest and external signal that the sensor was receiving, which adds a noise.
![screen](../images/module5/screen_magnet.png)



**Important advice**: When uploading the video on git via the markdown file, pay attention to put a supplementary space ../ before the actual local repository.
On the contrary, no one could watch your videos.


## Useful links

- [Arduino site](https://www.arduino.cc/)
