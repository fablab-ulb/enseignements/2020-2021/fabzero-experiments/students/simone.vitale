# 3. 3D printing





## Week resume



**Objectives**:

* Learn more about the conception of 3D images, merging different projects and designing a flexible structure.
* Improve the knowledge on OpenScad and FreeCad following the designs of other students.
* Improve my documentation.
* Print 3D models using a Prusa printer at the FabLab.
* Learn about the settings of the Prusa slicer software, which precedes the printing of the object.
* Learn more about the  Licenses on codes.
* Apply the notions of the  Licenses on codes.



## 3.1 Lessons in the FabLab

I have followed a lesson, provided by **Quentin Bolsee**  on how he built and projected its machine for [his FabLab project](https://fabacademy.org/2020/labs/ulb/students/quentin-bolsee/projects/final-project/3-development/).  
His project was really inspiring and consisted in a mechanism to scan 3D objects.
It is really interesting to learn the conception process of this machine and it has transmitted me some hint for my future project.  
In particular, the important part is the design of the project and the difficult to predict the problems which could present.


### 3.1.1 What is a Compliant mechanism
I have also been introduced to compliant mechanisms.  
They are based on flex links in order to provide a structure to excercise forces and  mechanical inputs.  
A task of this week has been to develop one of this structure merging our OpenSCAD design with the other projects, properly citing the sources.  
More information about the compliant mechanism can be found [here](https://en.m.wikipedia.org/wiki/Compliant_mechanism).

### 3.1.2 The 3D printing, the Prusa software

The availability of 3D printing is the base of the Fablab.  
Cheap machines and at high availability in order to realize the project that have been conceived.  
During 3D printing, a phase change occurs in the filament material as a result of its heating (above the melting point), melting in the
nozzle, and the process of solidification after extrusion leads to the final piece of the material.  
For the moment, the printing could only be made at low scale due to the high time of realization of the pieces.  
In fact, my two pieces have been realized in 60 minutes and there will be a great challenge to pass to a larger scale manufacturing.


The **.stl** file from any 3D modeling software (OpenScad, Freecad,etc) is imported in the software which divides in slices the piece.  
Particular attention must be dedicated to the printing parameters, which must couple with the chosen materials.  
The open-source Prusa Slicer software has been used in our case, with the creation of the Gcode, the standard output file accepted by the slicer.

The printer then will pose the material on the platform of work slice by slice in order to form the designed piece.


My source to learn about 3D printers has been [the FabLab site](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-experiments/class/-/blob/3492f57983b21259d80457abc2f78fa86a1e6420/vade-mecum/compliant-mechanisms.md).

I learned that, when we decide to print something in 3D, it is important to take into account the elasticity curve.
In fact, when going over the linear zone of deformation, the object could eventually break.

The program which is used for 3D printing is the Prusa Slicer software.
An important resource to learn about it has been [the following site](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-experiments/class/-/blob/master/vade-mecum/3D_print.md).

The software can be downloaded from the site of [Prusa](https://www.prusa3d.com).

## 3.2 My first experience with the 3D printer

### 3.2.1 Tutorial step-by-step on how to print
These are the major step I made using for the first time the printer:

* I coded [my first version of the program](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-experiments/students/simone.vitale/-/blob/master/docs/kit/cantilevier_beam.scad) in OpenSCAD. I have discovered later that the program had some serious problems and I have solved it in the [right version](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-experiments/students/simone.vitale/-/blob/master/docs/kit/cantilevier_beam2.scad).

* I then created the stl format, which is easily accepted by the Prusa software and it is the most common version to present the design of a project.
The two files I have printed are
[the old version](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-experiments/students/simone.vitale/-/blob/master/docs/kit/cantilevier_beam.stl) and  [the right version](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-experiments/students/simone.vitale/-/blob/master/docs/kit/cantilevier_beam2.stl) in **stl**.

* I have then worked on the Prusa platform. At this moment it is fundamental to know which is the printer you are going to use.  
In particular you should know the **model** and the **resolution** of the machine.
I decided to use the _MK3_ or _MK3S_ Prusia printer with the resolution of **0.2 mm**.

I practiced two different approaches on the machine:

* My **first approach** to the machine was to test its limits and its behaviour printing my model without modifying any setting of the machine.  
So, I validated my project as shown in the screenshot
using [the old version](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-experiments/students/simone.vitale/-/blob/master/docs/kit/cantilevier_beam.stl).
I then clicked on the "Decouper maintenat" command in order to obtain a **G-code**.

![Example](../images/module3/mod1.png)

The G-code must be then copied on a SD card and then put in the printer.

**Pay attention**: the print will read your card only if it is on the opposite verse.

Then, I selected my file on the printer screen and the printing started.
My only difficulty has been that I had to change the filament of the printer and it required some effort.
The result I obtained is the following:

![Example](../images/module3/printing1.jpeg)

The piece has been printed in PLA but other different materials could be used.
In particular they are usually plastic-derived(ABS) or carbon-derived, the latter result in an high resistance.

The first thing I did was trying to flex it, no way....
However I didn't loose patience and start analyzing my piece considering each part.

### 3.2.2 Analysis of the first piece

#### Good points

* The holes, with rayon 2.5mm were perfect and fitted the lego adaptation.
* The piece was really well-structured and solid.  Moreover, the printing has been extremely well developed.
* The geometrical structure was perfect, even though there were some problems on parameters.

#### Improvement points
The central part was not flexible at all.  
So, I thought about how to solve the problem and I decide to reduce the height of the structure.  
In particular the height of the two blocks with the holes has been divided by two in order to permit the union of different legos.
Moreover, the height of the two blocks around the central block have passed to 0.3mm.  
I have in that way tested the machine and its sensibility.  
I have also, under the advice of my mentor, modified the setting **couches solide** in order to make the structure more flexible [in this command](https://help.prusa3d.com/en/article/layers-and-perimeters_1748) which is accessible in the expert settings.  
I put it up to 1 in order to test the resistance of the material and the structure is still stable even if it has become a lot more flexible.
![](../images/module3/setting_prusia.png)

I have also modified part of the Openscad code because the distance between the holes should be 8mm.  
Instead, no connection of more than one hole could have been possible.  
So, the parametrisation has taken into account the fact that, for every N_holes possibles(imposed by the lenght of the object), the distance between the holes should be of 8mm.
I show here the comparison of the two print, where the second **stl** [file](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-experiments/students/simone.vitale/-/blob/master/docs/kit/cantilevier_beam2.stl) has been issued from the program modified as described before.


![](../images/module3/printed_comparison.jpeg)


#### Improvements on my flexlink which have been applied

However for **my flexible kit**, I am going to apply the following improvements:

* I have increased the value of the variable **bottom_cube_height1** to 0.6, the height of the two little cubes at the center, in order not to make them so thin to break.
In particular, my flexlink had broken after one week due to the small height of the piece.  
* I am also going to reduce the space between the holes to 8mm, as suggested in the site of Lego, because it does not allow to insert the legos in more than two holes.  
* I also modified the length of the object in order to fix it as the **N_holes * 8mm** in order to allow to fix the connection in every hole if wanted.
The value 8mm is issued from the distance between the different holes.




## 3.3 My flexible project

As a start I have decided to design my compliant mechanism project starting from the union of different flexibles.
I had the task to create a kit of flexlinks.
In particular I have been interested by the following structures taken from the [official site](https://www.compliantmechanisms.byu.edu/flexlinks):

* The [Fixed-Fixed Beam - Initially Curved ](https://www.thingiverse.com/thing:3016949) has attracted my attention because of the possibility to fold the piece and create more dinamical structures.  
For this program I have adapted the code of Paul Bryssinck in its [repository](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-experiments/students/paul.bryssinck/-/blob/master/docs/CADFiles/FixedBeam-InitiallyCurved%20.scad) and I have modified the number of Holes and the width in order to use it in my flex link kit.
The code I propose, and properly cited, is reported [here](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-experiments/students/simone.vitale/-/blob/master/docs/kit/FixedBeam-InitiallyCurved%20.scad) and its stl format is [here](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-experiments/students/simone.vitale/-/blob/master/docs/kit/FixedBeam-InitiallyCurved%20.stl)

* The [FlexLinks: Fixed-Fixed Straight Beam ](https://www.thingiverse.com/thing:3020736) because it allows the rotation of legos.
In this case I have also adapted a code from Paul Bryssinck and its [repository](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-experiments/students/paul.bryssinck/-/blob/master/docs/CADFiles/FSBeam-straight.scad).
His codes were perfectly coded and so I just modified the parameters and properly cited his work.
Here is my [scad code](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-experiments/students/simone.vitale/-/blob/master/docs/kit/FSBeam-straight.scad) and my [stl code](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-experiments/students/simone.vitale/-/blob/master/docs/kit/FSBeam-straight.stl) for the **Fixed-Fixed Straight Beam**.

* Another flexlink I add has been designed by [Floriane Weyer](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-experiments/students/floriane.weyer/-/blob/master/docs/FabZero-Modules/module02.md).
It makes reference to the following flexible: the **Cross axis flexural pivot**.  
This has been the most important learning path of my week.
In fact, due to the lack of its final code, I have had to work on its old project which presented some bugs to correct.  
The clear explanation of the code has made the task quite easy.
In particular, in class we have discussed on the fact that the two cubes which connected the cylindrical parts should not be merged together as the 3D printer would have done in the first interpretation.

![first interpretation](../images/module3/piece_croix.jpeg)

So, I operated on the code with a vertical translation of the two bars in order to obtain the following form.

![my interpretation](../images/module3/croix_mine.jpeg)

Finally, I noticed that there were some warnings on the definition of cylinders and cubes when the command _center=True_ was used.  
However, when I modified it in _center=true_ the design was not correct at all.
So I have understood that the software considered the command as **false** because the first T was capital, which was actually the correct command for the code instead of **true**.
Here is my [scad code](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-experiments/students/simone.vitale/-/blob/master/docs/kit/cross_axis.scad) and my [stl code](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-experiments/students/simone.vitale/-/blob/master/docs/kit/cross_axis.stl) for the **cross axis**.



* The **Fixed-Fixed Beam - out of plane** has attracted my attention because it grants more freedom in the construction of the kit permitting to link in horizontal way.  
It is really similar to the **initially curved**, because it just involves a rotation of 90 degrees of the cylinders.
For this program, I have adapted the code of Paul Bryssinck in its [repository](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-experiments/students/paul.bryssinck/-/blob/master/docs/CADFiles/FFBeam-OutOfPlane.scad).
The code I propose, and properly cited, is reported [here](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-experiments/students/simone.vitale/-/blob/master/docs/kit/outofplane.scad) and its stl format is [here](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-experiments/students/simone.vitale/-/blob/master/docs/kit/outofplane.stl).  
It is needed a support to print it.

### Analysis of my work

During the second week of my FabZero academy, with the aim of working in spiral, I have been working on a simple flexlink which I have built alone(inspiring from an existent piece).  
Therefore, following the advises of my professor, [in his blog](https://dtwg4.github.io/blog-flexible-jekyll/techniques-gestion-projet/) I have followed the **spiral process**.  
This week I have been able to interact with other codes from my colleagues and I have understood the logic they apply and the different coding techniques to create, for example, a curved rod.

![breakpieces](../images/module3/mine_twoways.jpeg)

### Prusia visualisation of the flex links and tutorial on the Prusa Slicer software

I decided then to import each piece I am going to print for my flex link kit in Prusia software in order to  visualize the general form of my kit and eventually study some configurations.
![](../images/module3/kit.png)
I will profit of this module to propose a brief analysis on the Prusia software with reference to the different elements I decided to print.
Before printing and even before uploading the **stl** file in the **Prusia slicer software** we need to consider that the following parameters have been already settled via OpenScad in order to be _Lego compatible_:

* Dimension of the flexlink in each dimension.
* Distance between the holes which must be 8mm.
* Imagine the flexibility of the object and adapt the size of each element.

### 3.3.1 Tutorial on the software

The following link on the [steps to follow](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-experiments/class/-/blob/master/vade-mecum/3D_print.md) has been the base of the following Tutorial.

#### How to choose the printer and the material
The printer I have chosen is the  **Prusa I3MK3** or **I3MK3S** with a measure of 0.4 mm (the measure can be adapted in the Prusa software).

![prusa_printer](../images/module3/printer_setup.png)

Another important part is the **choice of the filament** which has been made in the following menu, it corresponds to the material (**PLA** in my case) followed by the size of the printer filament divided by two (**0.2mm** in my case) .

**Advice**: put the setting of the printer in expert modality in order to choose every parameter.

I import an **stl** code from: ![here](../images/module3/importprusia.png)



### Settings for the printer

My choice of the setting has been as it follows .

![impression2](../images/module3/setting_prusia.png)

##### Density
The density is usually settled as 10 or 15 %, maximum 35 %.
![impression2](../images/module3/impression_reglages.png)

##### Support
The "jupe" draws the perimeter of impression.
"Bordures" can be put to support objects which are vertically extended, we will see an example of its necessity in some sections.
In particular it determines the degree of sticking of the flex on the plate.


### Creation of the G-code
Then, the Gcode can be exported and this was an image of my impression.



### 3.3.2 Result of the printing process
Here I propose a photo of the different flexs printed in order to describe the strength and the defects of my printing process:

![printing](../images/module3/complete_set.jpeg)

* The [Fixed-Fixed Beam - Initially Curved ](https://www.thingiverse.com/thing:3016949) is perfectly printed with the right measurements and I noticed that it could be useful to link.

* The [FlexLinks: Fixed-Fixed Straight Beam ](https://www.thingiverse.com/thing:3020736) is well printed and it allows a 360° of rotation.

* The **Cross axis flexural pivot** is perfectly printed and the movement is satisfactory.

* The **Fixed-Fixed Beam - out of plane**, instead, has given many problems on the printing process.  
It could be seen in the holes, which are not closed because the filament lost its trajectory near the end of the printing and expulsed my flexlink.  
It has been given by the lack of bordures and, given the fact that my piece was unstable in its position and not enough sticked, it fell on the plate.  
Therefore, an important advice on the vertical link could be to avoid the printing of more than two pieces at the same time.
In fact, the problem on the **out of plane** could have endangered my other pieces.

* Finally, an important discussion can be done on the piece I conceived:  
As a first consideration, the flexlink works really well after the printing process.
Although, after some hours of utilization it breaks, as I show in this photo.  
I have printed two models with different heights(0.1mm and 0.6mm) but they both do not work.  
I talked to my professor and he advised me to use in this case a more flexible material than **PLA**, and to use instead **PETG** or **Flex** which would grant more flexibility of my piece.
I will print in that way next week and I will upload here the photo.

### 3.3.3 Improvements along the weeks
The successive weeks I have decided to apply some improvements in order to solve some problems I had taken into evidence in this module.
At the end of this process each flexlink had been printed in the right manner.

* The **out of plane** has been printed using the following border configuration, which sticks the piece on the plate.
![impression](../images/module3/bordure2.png)
The final result was the following, confirming that everything has been rightly parameterized in the setting.
![impression](../images/module3/outofplane_print.JPEG)
* For the flexlink I have been conceived, the solution has been to print it in the **flex material**.
In order to do this I have opened a new filament, paying attention to cut the first part as it is shown here.
![impression](../images/module3/flex_open2.JPEG)
After the recharge of the filament I had to modify the Prusia slicer parameters in order to change the filament.
The parameter can be easily changed in the main page.
* I have also realized some other pieces, such as the Fixed-Fixed Straight Beam in flex.  
**Caution on the flex**: It must be noticed that the flex is really different from PLA, in particular it could happen that the printed result does not correspond to the initial design.
Therefore, test a little before using it.
* I have also printed the cross_axis pieces in order to test the flexibility:
![plate](../images/module3/flex_crossaxis.JPEG)
* I have also realized some flexlinks in **PETG** in order to try this material.
The material has been used for the printing of the following pieces:
![plate](../images/module3/plateau.png)

The following result has been obtained:
![](../images/module3/petg.jpeg)
I can really advice this material because it results more flexible than the PLA.

## 3.4 Examples of Compliant Mechanisms

### 3.4.1 Test on simple compliant mechanism
It has been asked to produce a video, with the length of seconds and the dimension minor that 10Mb, in order to shows how our flexlink mechanism worked.
The limitation on the dimension is given by the fact that the server could not be overwhelmed with too many videos of big size, creating like that some technical problems.
Therefore, in order to reduce videos, I have used the following command, inspired from the first tutorial on documentation and the [Stephanie Krins page](https://fablab-ulb.gitlab.io/enseignements/2020-2021/fabzero-experiments/students/stephanie.krins/FabZero-Modules/module03_imp3D/):

```
ffmpeg -i input.mp4 -vcodec libx265 -crf 28 output.mp4
#I also applied the following application to rotate my video:
ffmpeg -i input.mp4 -vcodec libx265 -crf 28 -vf "transpose=1" out.mp4

```
I have started from videos of 6Mb and I have uploaded at the end a total size of 6Mb in videos, which shows how the compression has been useful.

In order to upload your local videos, without sound, on markdown the following structure from html can be used:

```
<video controls muted>
<source src="../path" type="video/mp4">
Video description
</video>
```
**Important advice**: When uploading the video on git via the markdown file, pay attention to put a supplementary space ../ before the actual local repository.
On the contrary, no one could watch your videos.
With this sintax, you will not be able to visualize your videos in Markdown preview, but only on the site.
In order to test in local if it is the right video, just delete the ../ before the path.

**Important notice**: When using ffmpeg, the videos have been well reduced in sizes. Nevertheless, when uploaded on gitlab, they were not visualised as if they were corrupted.
I have solved this problem using online sites to compress my videos.
It must be underlined that ffmpeg videos are readable in local.

At the start I thought to create directly a complicated structure using my flexlinks.
However, I observed, following the spiral thought , that it would be useful to start with some simple video-tutorials on the single flexlinks and then, to complicate the structure.



I have started to test the  the **initially curved**, in which I have appreciated the elasticity of movements.

<video controls muted>
<source src="../../videos/Flexlinks/initiallycurved.mp4" type="video/mp4">
</video>



I then tested the **FixedBeam_straight**, printed in flex and observed the facility of movements:


<video controls muted>
<source src="../../videos/Flexlinks/beam.mp4" type="video/mp4">
</video>

I then tried to mix the cross axis and the beam straight in the following configuration:

<video controls muted>
<source src="../../videos/Flexlinks/complete1.mp4" type="video/mp4">
</video>

### 3.4.2 The more complex compliant mechanism, the human jaw
After having tried this kind of configuration, I made some research about some compliant mechanism.  
The **compliant mechanism** is a mechanism is a mechanical device used to transfer or transform motion, force, or energy. Traditional rigid-body mechanisms consist of rigid links connected at movable joints.
Some interesting examples can be found on [the official site of BYU](https://www.compliantmechanisms.byu.edu/gallery) .
Nevertheless, I decided to base on one of the most important compliant mechanism of our body, the jaw.  
I then decided to create, adding to the precedent pieces I made the cross axis with a single link instead of two, which can be easily found on the Paul's repository.
The conception of the jaw has followed from the interesting elements I discovered about the single flexlinks and every piece has its role.  
As a matter of facts the following elements are composing it:

* The orange flexlinks allows to recreate the posterior part of the jaw. Its stability allowed to make it fix.
This part is capable to absorb stress when there is an horizontal compression and it results stable after some tests.
I have discovered it in the [Floriane's module 2](https://fablab-ulb.gitlab.io/enseignements/2020-2021/fabzero-experiments/students/floriane.weyer/FabZero-Modules/module02/).
* Starting from the posterior part, and having a symmetric structure, the system has two cross axis on each part.  
The first one is made on PLA, so it is solid and can support some stress. The second one allows the vertical flexibility of each part and is made in flex in order to be flexible and share the stress on the other parts of the structure. Obviously, it is the most probably piece which could break.
* On the superior part I have placed a beam straight printed in Flex, which allows horizontal movements, in analogy with the real jaw.
* Finally, the inferior part is made of a out of plane printed on PLA, which is useful for creating a blocking mechanism at the base.






<video controls muted>
<source src="../../videos/Flexlinks/human_jaw.mp4" type="video/mp4">
</video>


## Useful links

- [Prusa slicer](https://www.prusa3d.com/drivers/)
- [Google](http://google.com)
- [Markdown](https://en.wikipedia.org/wiki/Markdown)
