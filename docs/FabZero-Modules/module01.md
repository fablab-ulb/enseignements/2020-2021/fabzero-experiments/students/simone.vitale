# 1. Documentation



## Week resume



**Objectives**:

* Integrate in the Fablab group of the course.
    * Develop a collaboration with the others
    * Not to be afraid to ask
    * Improve informatics skills
* Learn how to use the following software(Git, GitLab, Markdown, Bash, Mkdocs, etc.)
* Make a documentation on my work
* Install Linux.







This week I started to get used to the documentation process and learned the most important software which will be useful for my course.  
In particular I have started my learning process from installing Ubuntu 20.04 in my pc, using a partition.
I have followed the documentation on [the Ubuntu site](https://ubuntu.com/download/desktop).  
I think it is really important in order to better become familiar with the Linux environment.
Although, I have experienced some problem after the installation because of an error on the Nvidia packages.  
In particular, after the suspension of the PC, the screen stops working with some reference to errors in the **modeset**.
In order to solve this graphical problem, which luckily does not affect my working on the project, I have consulted [the Nvidia site](https://www.nvidia.com/fr-be).  
My problem regards the Ubuntu interpretation of the driver 460 which seems corrupted.
However, after some internet research, it is clear that it is a widespread problem which should be solved in the future.



## 1.1 The version management
For the description of my project and of the modules, we have based our programs on the Git management.  
Even though our project could be thought as a "working alone project", I think that it is really important to take confidence with Gitlab.
In fact, it allows us to manage later a bigger project.  
Let's imagine hundreds of people working on a project, how can we synchronize each modification?
The solution is the use of a version management software, which tracks the version which are proposed and allows for the manager of the project to analyze each modification and choose which is the best during the process of merging.

The main advantages for the use of a version management are:

* Easy interface for the interaction with the project.
* The codebase can be easily shared.
* Your files are stored in a cloud.
* You can take confidence with the linux system and the terminal.

The ```repository```  is a server where your files can be stored online, a sort of cloud.
 The service that gitlab/hub are proposing is some server space with version management software ready to go.



Here I resume the most important git commands to apply when managing files in the repository:


```
git pull
```
Checks for remote conflicts, download latest deltas from the selected branch

```
git add .
```
Adds all new files, however in order to track the modification another step is needed:

```
git commit -m "Modification"
```


When you start experimenting with the code, you should create a branch and select it.
The principal branch is the **master branch** in our case.
The ```merge``` command allows to merge branches:

```
git push origin master
```
This last command is to push our file.
A first-user error I made was expecting that, when pushing the file, the site will be updated instantly.  
Instead, you should launch a job from the git site with a certain schedule where you compile your site via mkdocs.

See documentation for all flags suited for your needs. This is far from an exhaustive list.


Another useful tools of mkdocs allows to view our modification of the web pages without the need to wait the 2 weekly schedules where our files are sent to the server.  
It helps to prevent our site from errors.
For example, the Markdown visualization can compile some text which is then considered as an error by mkdocs.

The following command can be applied in order to generate a url to consult which is updated real-time:
```
$ mkdocs serve #permet d'afficher le site dans un navigateur
```

### 1.1.1 Git


Git is a fundamental element of my week learning because I have learned the easiness to operate on a given site(and surely having the permissions!) and modify it applying some command line.

#### Install Git
For the installation of Git I have followed the [following tutorial](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html#install-git ) provided by [the Fablab academy](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero/basics/-/blob/master/documentation.md#gitlab), where is it clearly explained how to clone a site and how to create an SSH key.
Moreover, I have learnt from the [Nicolas De Coster site](http://archive.fabacademy.org/2018/labs/fablabulb/students/nicolas-decoster/?page=assignment&assignment=01#InstallUseGit) how to use it.

#### Resume of the most useful commands
The most important commands that must be used each time we are working on a file are:

```
# To download the latest changes in the project
 git pull <REMOTE> <name-of-branch>


#To add all changes to commit(which means to effectively send to the site)
git add <file-name OR folder-name>
git commit -m "COMMENT TO DESCRIBE THE INTENTION OF THE COMMIT"  #where it is possible to describe the changes.


#Send changes to remote server:

git push <remote> <name-of-branch>
#the usual command to apply is:
git push origin master   #where origin indicates the precedent repository used and master is the branch which is used.
#When working in a shared project this command enhance to verify the changes which have been applied.
git diff
```

#### Main problems for first users
As a first user, I have experienced some problems before understanding how it worked.  
As a matter of facts, I propose here some advises:

* A first user should try to just modify the text locally and then upload. If you edit online, do not forget to pull your files from the start.  On the contrary, you could generate some conflict between files.  
If it has happened, it is however quite easy to manage your files opening them and then git automatically makes you choose the version you want.
As a following step, someone could commit your project in a second branch and could send you some suggestions.
In that case, when you will pull your files, a message will be shown suggesting to manage your files.
* When you commit your own project, or use the webIDE of git, you will not be able to upload locally until you manage the modifications.
* About images manipulation, do not forget that next to the software I will suggest in the following sections, there are many others. Sometimes, when having trouble with a software, an online image or video compressor could do the job saving you some time.  

## 1.2 The Markdown language

I have discovered for the first time this language which I found easy to learn and really useful for documentation.  
This is the language which is compiled by mkdocs, a python-based language, in order to give a page site which can be consulted.  
In particular I had been used to **latex language**, which is strongly used to write thesis, so this was my first experience with the markdown.
An important characteristic is that you can mix **html language** and **markdown** text in it.  
A brief tutorial on markdown is at [this link](https://www.markdowntutorial.com/ ) and it allow to ease with this language.
Other utilities can be found at [the gitlab descripiton](https://docs.gitlab.com/ee/user/markdown.html).
I have been able to appreciate some Markdown skills:

* It permits to easily organize paragraphs and make references to online documentation as it follows:
```
[name of the site](site)
```  
* It is really easy to join an image using the following command :
```
![name of the photo](\path\photo.format)
```  
However, I have experienced an interesting problem regarding the path for the files.
Here I write a little **advice**: if it does not find your photo, it means that the path is wrong and maybe you should start from the git repository and then find the right directory.
I show here an example:
```
![name of the photo](\..\images\photo.format)
```
* Titles can easily be done using the # command which allows you to decide the format of the title using the bigger format with the following sintax "#Title" or a little one "#####Title"

Theo Lisart, a student, has also written a tutorial on how to introduce latex notation for equations in Markdown, check it out [here](https://fablab-ulb.gitlab.io/enseignements/2020-2021/fabzero-experiments/students/theo.lisart/#Tutorials/latex_in_mkdocs/)

The **latek** can be used in markdown using the following sintax:

* The text can be written using the standard sintax on the line
```
\( text latex  \)
```
 which gives \( text latex  \).
* The mathematical formulas can be written using the sintax that must be respected:
```
\[ math mode  \]
```
Here I show an example with an important formula in physics, the Newton law of forces:  


\[ \textbf{F}=m \textbf{a} \]

No text can be written next to the formula in the same line and it should be divided by two spaces from the other text.

## 1.3 Ubuntu terminal and bash
I had already some learning experiences on the terminal, either in Windows and Linux, but I found really interesting to learn something more on the [official tutorial](https://ubuntu.com/tutorials/command-line-for-beginners#1-overview) provided by the Ubuntu site.  
I have also written a list of the most useful command in the terminal.

```
$ pwd #Print Working Directory

$ ls # List the elements the  working directory

$ cd <path> # to move into the directories
$ cd ..  # to move back to the precedent directory



$ mkdir <name> #to create a directory

$ mv "<file>" <directory> # move an element to a directory

#if an editor is used(for example atom):
$ "editor" "name_text" permits to open and to modify the text   

#Sometimes if some permission problem emerges, it could be useful to use the chmod command:


$ chmod+x <directory> #changes the permission on a file but it is possible to add other commands following the documentation.
$ stat -c %a <directory>   # It permits to identify the permission on a single directory.

```




## 1.4 Video and Image manipulation
There are several software for images manipulation(compression,modification,resize).
In particular I use the following:

* FFmpeg
* Imagemagick
* Gimp

### Gimp
This week I have downloaded and installed the Gimp software, which is really useful to resize images and it has a simple graphic interface.
I have followed the instructions on the [Gimp site](https://www.gimp.org/downloads/) and the tutorial on the Gimp site.  
Nevertheless, when manipulating more than two images, it could be not so smart to use it.


### Imagemagick

I have followed [the following tutorial](https://www.tecmint.com/install-imagemagick-on-debian-ubuntu/) to install it.
It could be really comfortable to apply the same command on different images with the command:
```
mogrify -resize 960x528 *.png
mogrify -quality 70% *.JPEG
```
The files must be in the same folder and of the same type.
Another interesting command is "-quality 50%" where you can decide to reduce your quality(and so the size of the photo) of 50%.  
**Pay attention**: do not exaggerate with the number, usually when arriving at 30% the photo loses part of the contours of the objects and becomes flux.

The installation took about 20 minutes so do not think something is going wrong.

**Error in the installation**
I have installed the software from the Ubuntu repository as it follows:
```
sudo apt install ImageMagick
```
However I had the following problem, when apllying the command on an image Jpeg,png etc.. the format was not recognized.
The following error appeared:
```
no decode delegate for this image format `JPEG' @ error/constitute.c/ReadImage/572
```
So I looked on internet and [I found this site](https://askubuntu.com/questions/745660/imagemagick-png-delegate-install-problems) which solves the problem step by step.  
Pay attention on the first command because the packages will change the time you apply them.
In my case I had to remove the 12 from the first package and so my command passed from :
```
sudo apt-get install build-essential checkinstall \
             libx11-dev libxext-dev zlib1g-dev libpng12-dev \
             libjpeg-dev libfreetype6-dev libxml2-dev
```
to
```
sudo apt-get install build-essential checkinstall \
             libx11-dev libxext-dev zlib1g-dev libpng-dev \
             libjpeg-dev libfreetype6-dev libxml2-dev
```
The last command too requires to write the good version because the software is directly downloaded from the official site.  
For me it ran like that:
```
wget https://www.imagemagick.org/download/ImageMagick-7.0.11-2.tar.bz2 && \
tar xvf ImageMagick-7.0.11-2.tar.bz2 && cd ImageMagick-7.0.11-2 && ./configure && make && \
sudo checkinstall -D --install=yes --fstrans=no --pakdir "$HOME/imagemagick_build" \
     --pkgname imagemagick --backup=no --deldoc=yes --deldesc=yes --delspec=yes --default \
     --pkgversion "7.0.11-2" && \
make distclean && sudo ldconfig

```

### Fmpeg

It is an interesting feature to reduce the size of your videos.
It can be installed via the code:
```
sudo apt install ffmpeg
```
I have used it to compress videos via the following command:

```
ffmpeg -i input.mp4 -vcodec libx265 -crf 28 output.mp4
```

I also applied the following application to rotate and reduce the size of  my video:
```
ffmpeg -i input.mp4 -vcodec libx265 -crf 28 -vf "transpose=1" out.mp4

```

Sometimes the compressed videos are not visualized in git when uploaded.  
In that case, I advise to use other software or online providers.

## Useful links

- [Gimp](https://www.gimp.org)
- [Google](http://google.com)
- [Markdown](https://en.wikipedia.org/wiki/Markdown)
