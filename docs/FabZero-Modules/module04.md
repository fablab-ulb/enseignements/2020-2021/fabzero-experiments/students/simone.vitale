# 4. Laser cutter and vinyl cutter


## Week resume


**Objectives**:

* Learn  about the conception of the 2D images on the Inkscaspe software.
* Document my learning process of the laser cutter, developed in group of 3, where we have characterize our laser cutter's focus, power and the speed parameters for folding and cutting cardboard paper.
* Improve my documentation.
* I have designed a kirigami to make a sample 3D object designed by me.
* Learn about the settings of the vynil cutter and how to use it.
* Apply the notions of the  Licenses on codes and objects.



## 4.1 Computer Controlled Cutting, the logic behind

The aim of the computer cutting is to conceive and design a 2D project via a software, for example **inkscape**(already in 2D and non parametric) and **OpenSCAD**(we design parametrically in 3D and then make a projection in 2D).

The numerical format, which is usually introduced in 2D conception, is the SVG(alias for scalar vector graphics), which conceives images as a vectorial path.  
The path is then interpreted by the laser cutter in order to cut the desired form.  
This technology allows also to the repeated cutting of objects, which saves time and allows the creation of fantastic objects as I will show.  
For this module it has been chosen to design my kirigami in **Inkscape**.
The spiral learning is conceived to approach the parametric form another time.
The vynil cutter allows to decide which force is applied on a object, which is an important characteristic of the machine.


## 4.2 Inkscape

### Inkscape installation

To install Inkscape I have written these commands on the terminal, taken from the [official site](https://inkscape.org/release/inkscape-1.0/gnulinux/ubuntu/ppa/dl/):

```
sudo add-apt-repository ppa:inkscape.dev/stable
sudo apt update
sudo apt install inkscape

```
The platform is quite easy to use because you can draw every form you want.

### Design of my kirigami
We have been given the task to create a kirigami in order to test the inkscape software.
For that I have made use of [the official tutorial](https://inkscape.org/doc/tutorials/basic/tutorial-basic.html#:~:text=Yet%20another%20convenient%20way%20to,will%20assign%20the%20stroke%20color).

The first thing I learned was how to trace a line and how to color it.

![line](../images/module4/line_color.png)

I have decided to design the following figure inspired from [the youtube tutorial](https://www.youtube.com/watch?v=k1BH_FEUZWo)(really interesting) on how to design **a spiral kirigami**.  
I have decided to dedicate on this kirigami because I found it really mathematically based.
The aim of the tutorial has been to first create the structure of base:  

![base](../images/module4/base.png)

The structure has then been propagated duplicating and rotating the structure of an angle of 3.7° on the horizontal to form a 5 squares' spiral.

### 4.2.1 The first version
#### Errors in the first version
The tutorial has made me learn some important skills:

* create always a grid in order to identify well the points.
* Apply the command of group and ungroup in an intelligent manner in order to manage the design.

![group](../images/module4/ungroup.png)

* it is important to study the structure you are going to design.

In the case of the spiral, there are important mathematical considerations which are applied after that the base of 5 squares has been formed.  
I had to test which scale should be applied when duplicating the structure and the rotation to apply.
In my case the scale has been obtained confronting the size of the horizontal lines woth the obliqual lines projected on the horizontal axis.
In my case I obtained a scale of:

\[ scale=(\frac{x_{obliqual}-x_0}{x_{base}-x_0})  =0.91011\]

So each time I took the base as in figure and in the section "object-transform" I modified the rotation in +3.76° and the size of 91.011.

My first essay was problematic because I forgot sometimes to put my grid to identify the points and it is done using the command **edit path with nodes**.  
As it was said in the tutorial, it has provoked some imperfections due to the propagation of errors in the duplication for a bad alignment.

The main **Lesson for the future** is not neglect propagation of errors.  
The following form has been obtained after the first design:
[File svg for the first version](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-experiments/students/simone.vitale/-/blob/master/docs/kirigamiandlaser/kirigami_to_print.svg)


### 4.2.2 The final version
I have then restarted and made more attention on the different points of conjunctions.  
At the end I obtained the following figure:
[link to the definitive svg file](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-experiments/students/simone.vitale/-/blob/master/docs/kirigamiandlaser/kirigami_to_print2.svg)

![](../images/module4/final_spiral.png)

I have learned the basis of **inkscape** and that is what it counts over than errors.

**Little reminder**: the difference between a kirigami and origami is that the kirigami implies to cut and fold the material.
Instead the origami just implies the fold of the structures.

**IMPORTANT NOTICE**: If you decide to cut this kirigami, my tutorial could be useful to arrive to the final configuration.  
Nevertheless, the final form of the kirigami is not granted because the folding process had not been explained in the tutorial.  
The folding process is really difficult because there is no suggestion on how to do it, so I prevent you from an easy task in the folding.  
The final result has been obtained folding the black and red lines in opposite direction but the exact process of folding is not so easy to reproduce.  

## 4.3 Laser cutting learning

### 4.3.1 Introduction to the precautions to apply:
Alex Cornu, member of the FabLab, has introduced the different laser cutter in the laboratory.  
He has illustrated the differences between them and the safety measures in this really useful [link](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero/machines/-/blob/master/LaserCutters.md).  
I have resumed here the most important thing to know:

* Precautions:
    * Know which material you are using reading every time the paper annexed.
    * Activate the air compressor
    * Activate the smoke extractor
    * Visualize how to stop the machine in case of danger and the safety devices.
    * Stay near the machines for all the time of the cutting.
    * Do not watch the point of impact in order not to catch laser reflection
    * Wait before opening the machines that smoke is not anymore there.
* materials
    * multiplex , wood pieces
    * Acrylic, Plexiglass
    * Paper but not higher than 3cm
    * textile but pay attention to the vynil content of the tissue.

 * **Not** advised materials
    * plastics, because they melt easily.
    * metals
    * PVC, which could be confused with Plexiglass being both transparent
    * Vinyl, in textiles.

* Forbidden materials
    * PVC : acid smoke.
    * copper, reflect LASER.
    * Teflon (PTFE)
    * Phenolic resin.
    * Animal skin.


### 4.3.2 The calibration

The aim of the process of calibration on the laser cutters has been to test the machine, the software and how it works.  
The task has been to create a paper of calibration where to apply different kind of cuts, in terms of **speed** and **power**.  
It will be really useful in order to use the machine to print a **kirigami**.
The machine would associate a color to each combination of desired power and speed and this will give the possibility to apply the following actions on the material:

* Cut the borders to give an external structure on the kirigami.
* Do not cut completely, but instead reduce the height of the material to allow the folding of the structure.

Each material needs a different calibration, and calibrations are generally at disposition near the machine.


#### Epilog Fusion Pro 32

![image of the machine](../images/module4/fusion_laser.JPEG)


##### Specifics

 * Surface : 81 x 50 cm
 * Maximal height : 31 cm
 * LASER power(CO2) : 60 W

We have been divided in groups in order to observe how to use the machine and how to calibrate it.  
I have followed the calibration of the other printer, **The Full Spectrum Muse** .

However, I have observed attentively the work of the others on this machine and I have seen how to manipulate the Driveboard App, the control interface of the machine.

The format compatible with this machine are: **DXF**, **SVG** et **DBA**.

The manual is contained in this [site](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero/machines/-/blob/master/EpilogFusion.md).  
An example of calibration applied to the laser cutter is the following on the wood:  
![bois_calibration](../images/module4/calibrationWood.jpg)

However, the other group has developed another calibration on paper, about 3mm height.  
The result is the following:  
![fusion_calibration](../images/module4/other_calibration.jpeg)
The aim is to distinguish between the power and the speed that allow **engraving, cutting and folding**.
The units of the software, which was installed on the pc directly connected to the printer, are **mm**.

It is generally advisable to print just one time on an object, because repeating the same printing process could not correspond to the same results due to the precision of the machine.







#### Full Spectrum Muse

![image of the machine](../images/module4/laser_mine.JPEG)
##### Specifics

* Surface : 50 x 30 cm
* Maximal height : 6 cm
* LASER power(CO2) : 40 W
* CO2 LASER and red pointer(really useful).


##### Retina Engrave, the interface

Before using the machine it has been necessary to respect the safety rules and to learn the control interface.

The safety rules are really similar to the Epilog Fusion because it is necessary to activate the air compressor and the machine to remove the smoke.

The connection to the software can be made via two ways:

* Connect your pc to the ethernet cable.
* Connect to the following Wi-Fi:
    * Wifi : LaserCutter
    * MDP : fablabULB2019

However, pay attention that you will be offline on internet whenever you connect to the line.
To access the interface you need to connect to http://fsl.local.  

The following formats can be used:

* Vectorial : SVG → the file will be imported in matrix form and vectorial.
* Matrix : BMP, JPEG, PNG, TIFF
* PDF (vectorial et matricial)


A tutorial on Retina Engrave is at [this link](https://www.youtube.com/playlist?list=PL_1I1UNQ4oGa0w55C772Y1mC6F4f3ZcG6).

For the settings the [site of Full Spectrum Laser](http://laser101.fslaser.com/materialtest) is a good source.

**Good points of the printer**: Although some limits in the power, the machine has some advantages:

* It is easy to use and the interface is clear
* The laser pointer helps the precision drawing.  
**Important setup**: When you turn off the printer it is important to focus the laser pointer.
It can be made with a cylindrical disk, which should be placed under the laser.  
Thereafter, the height of the laser can be set turning the screw until the point of the laser touches the disk.
However, when operating on the laser, do not create any activity on the printer in order to avoid incidents.  
![](../images/module4/calibrator.png)

* The software usually remembers the old projects(save however you projects).

 **advice**: the import of files could change the length of your draw(for example from 100 to 103), so pay attention to that for measures.


### 4.3.3 Examples of laser cutter and calibration of the Full spectrum muse

I have calibrated, with **Paul** and **Majda** the machine.
The material which will be used through this module has been paper.  
As a start we have designed a **calibration grid**.

The grid was visualized like that in the software:
![](../images/module4/calibrationPaperMuseLaserCutter.jpg)
The program to construct it is the following [program](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-experiments/students/simone.vitale/-/blob/master/docs/kirigamiandlaser/CalibrationPapermuseexport.re3).  
The grid shows how the cutter behaves with different power and speed.
The speed has its units in millimeter par minute but the units here are the percentages in confrontation with the maximum speed.  
The printed grid, which has been left for the future users, has been cutted as it follows:
![](../images/module4/calibrationPaper.jpg)  
The following information have been extracted from the test:

* To cut the paper we should put almost 3% of power and 60% of speed.  
* To fold the paper we should almost put 2% of power and 80% of speed.

**Important**: the speed determines the time of cutting, so its value is inversely proportional to the degree of cutting.  
In order to test everything we have learned we started to test some kirigami.
We started to test how to print and how to modify the parameters.
So, from the [following site](https://fablab-ulb.gitlab.io/projects/2020-fabxlive/workshop-mechanical-metamaterials/stretchable/), proposed by the teacher, we took and print two SVG file which could be easily generated.
The two kirigami are the **square bistable kirigami**,[with its svg file](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-experiments/students/simone.vitale/-/blob/master/docs/kirigamiandlaser/squareBistableKirigami.svg) and **the stretchable kirigami**, [with its svg file](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-experiments/students/simone.vitale/-/blob/master/docs/kirigamiandlaser/strechableKirigami.svg).

This has been our result with the square bistable krigami:

![](../images/module4/example2.jpeg)

The second kirigami, the stretchable, has been a deceiving because the best material in this case was a more elastic one.
In fact, paper tends to break when it is solicited to a stress.

![](../images/module4/example1.jpeg)


These screenshot shows how the second picture was visualized on the platform:

![](../images/module4/squareKirigami.jpg)



## 4.4 The printing process of my kirigami

I have decided to cut my spiral in the Muse printer, so I connected to the printer, opened the site and uploaded my svg file.  
I then decided to put the following cutting parameters on the printer:
![](../images/module4/essay1.png)
The result was really deceiving.  
In fact, maybe for a problem of calibration of the laser or for a problem in the parameters, the following cut has been obtained:
![](../images/module4/first_cut_spiral.JPEG)
I then tried to change my parameters, after having effectuated a second re-calibration because I changed of paper and I obtained, using the following parameters:
![](../images/module4/laser3.png)
The following output has been obtained:
![](../images/module4/spiral3.JPEG)
I then tested if the result could be similar to the one proposed by the tutorial.  
However it **did not work**, because the tutorial I have chosen did not describe the cutting parameters to apply and the way in which the spiral should be folded in order to obtain the result.  
Nevertheless, I really enjoyed the form I had created and I decided to continue with the help of the Fablab members.

### 4.4.1 Advices from the FabLab members
I then asked to Fablab members for improvements on my kirigami and the answer was magnificent.  
In fact the suggestion was to cut two times the paper:

* First time: use the same inkscape file and cut the black lines with the exact power for the folding.
* Second time: apply a mirror on the spiral and then cut the red lines on the other side.

This is the spiral with the mirror applied through Inkscape.
 ![](../images/module4/reverse_spiral.png)


I have then printed the front side of the spiral, giving the parameter in order to cut only the red lines.
![spiralforward](../images/module4/backward.png)
![spiralforwardprinted](../images/module4/a.JPEG)
Then, I used the comfortable laser light, which is included in the laser cutter, and it allowed to set the spiral on the backside and then impose the folding of the black lines.
 ![spiral_backward](../images/module4/laser_pointer.JPEG)
This second task is, however, complicated because in the second cut the spiral perimeter must be verified with the laser pointer with high precision(the Muse helps me in that task).  
Moreover the laser must be well calibrated in order to have a good result.


Therefore I uploaded on the server of the printer the mirror file and I imposed the following parameters:

* 100% speed and 2% power for the black and the blue
* 0% and 0% for the red lines.

This is the result I obtained on this part.
 ![spiral_backward printed](../images/module4/b.JPEG)
 The final result was however still deceiving and did not reproduced the form proposed.
  ![final result](../images/module4/spiral_try.JPEG)

Therefore, I decided to continue to experiment with the laser cutter and I reprinted the double-face kirigami paying attention to give **the same cutting parameters** of the precedent section and to place, when turning it, the object thanks to the laser pointer.  
The final result have been satisfying because the kirigami can now be folded and give a good result.
The result is the following:
![final result](../images/module4/final_kirigami.JPEG)

### 4.4.2 Improvements on my kirigami
Important improvements can be applied, in particular increasing the size of the kirigami.  
Moreover I should have started with a simpler form of kirigami instead of this one(I made exactly the opposite of the spiral process).




## 4.5 Vynil cutter

I have learned about how to cut using the Vynil cutter.
The brand of printer which was present is **Silhouette Cameo3**.  
Unfortunately there was no tutorial proposed on the Fablab Ulb on how to use it.
Therefore, I decided to test it myself.
 ![](../images/module4/printing_screen.JPEG)

I here describe the most important steps to follow applied on an example
### 4.5.1 The Silhouette studio software
On the [official site](https://www.silhouetteamerica.com/) a software can be easily installed on Mac and Windows.  
**No** software is present for linux, however an USB can be connected with the file ready to print.
#### How to place the material
Place the material on the printer support, stuck to the adhesive structure.  
Pay attention that the format of the support should coincide with the printer.
The right format is the following:
 ![](../images/module4/rightformat_vynil.JPEG)
Then load your paper using the touch screen.
 ![](../images/module4/vynil_screen.JPEG)
### 4.5.2 The first essay on paper
Finally, open the **Silhouette studio software** and import your file.  
The format it accepts are the standard images formats(JPEG,png,..).  
However, the svg file was not accepted.  
The stl file is nevertheless accepted by the software.  
When the file is loaded and centred, go to the "send" part:
![](../images/module4/section_to_print_silhouette.png)
In the line setting you can then set the force of the printing and the speed.
I have tried this kind of figure on paper in order to test the different powers and speeds.
In particular I noticed that on paper even at the maximum power it was difficult to really cut the geometrical form I desired.

### 4.5.3 Essay two, the stickers
I then tried to print a particular form on a sticker.
I decided to inspire from some interesting forms on the internet.  
In particular I downloaded the following two images and then decided to print the **snowflake** and the **panda** on a sticker.

The panda has been taken from [the following site](https://favpng.com/png_view/abstract-heart-shaped-pattern-panda-paper-craft-origami-kirigami-png/FENSCtyP), while the snowflake has been taken from [the thingverse site](https://www.thingiverse.com/thing:2733637).  
Therefore, I imported the figure in the software and then converted it in a printable form.
The material which has been used is the vynilmat and the parameters are present in figure.
![screen](../images/module4/vynil_flocon.png)
The following sticker have been obtained:
![snow](../images/module4/sticker_star.JPEG)
The studio file for the software can be found at [the following link](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-experiments/students/simone.vitale/-/blob/master/docs/images/module4/to_print_flocon.studio3).

The panda figure has been cut using the parameters 20 of force and 4 of speed and the following result has been obtained:
![panda](../images/module4/orso_sticker.jpg)

The studio file directly for the software can be found at [the following link](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-experiments/students/simone.vitale/-/blob/master/docs/images/module4/panda.studio3).
The panda has not been cut as expected.  
As a matter of fact, I have also cut the internal lines while I should have cut only the external forms and the internal figures.  
