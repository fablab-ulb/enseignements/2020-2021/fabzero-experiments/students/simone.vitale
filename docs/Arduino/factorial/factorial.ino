




/*
  Title Factorial.ino
  Author: Simone Vitale
  Date: 20/04/2021
  License: Mit license 
This code simply computes the factorial of the number factNum and print in in the serial.

Code issued from:  
https://github.com/BraveTea/Factorial-in-Arduino/blob/master/Factorial__borrowed_code_.ino
and modified in order to suit the Generic D11C14A board manager.
*/




int factNum = 4;

void setup()
{
  SerialUSB.begin(9600);
}

void loop()
{
  int result;
  result = factorial(factNum);
  SerialUSB.println(result);
  delay(5000);
  
}

int factorial(int n)
{
  if (n == 1)
  {
    return 1;
  }
  else
  {
    return n * factorial(n-1);
  }
}
