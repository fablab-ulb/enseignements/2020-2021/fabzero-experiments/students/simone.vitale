/*
   Title: Hall.ino
 Authors: Simone Vitale and Ellias Ouberri
 Date: 18/03/2021
 Licence: MIT license.

 Contributors: the code has been issued form the union of different examples with some debugging on the code.
- The HALL (HOLZER) MAGNETIC SWITCH MODULE VMA313 has been programmed using the user manual:  "https://www.velleman.eu/downloads/29/vma313_a4v01.pdf"  
- The example code for the ELECTROMAGNET MODULE VMA431 has been issued from the following page(user manual):"https://www.velleman.eu/downloads/29/vma431_a4v01.pdf" 


*/

int analogPin = A0; // potentiometer wiper (middle terminal) connected to analog pin 0
                    // outside leads to ground and +5V

int val = 0;  // variable to store the value read


/*
Turns on an Electromagnet on for one second, then off for one second, repeatedly.
This example code is in the public domain.
VMA431 connections
VCC = +5V
GND = GND
SIG = Digital line 0 (any line can be used)
*/
int Electromagnet = 0;
int LED = 13;
// the setup routine runs once when you press reset:
void setup() {
    Serial.begin(9600);           //  setup serial
// initialize the digital pin as an output.
  pinMode(Electromagnet, OUTPUT);
  pinMode(LED, OUTPUT);
}
// the loop routine runs over and over again forever:
void loop() {
  
  digitalWrite(Electromagnet, HIGH); // turn the Electromagnet on (HIGH is the voltage level)
  digitalWrite(LED, HIGH); // turn the LED on (HIGH is the voltage level)
  //delay(10000); // wait for a second
  digitalWrite(Electromagnet, LOW); // turn the Electromagnet off by making the voltage LOW
  digitalWrite(LED, LOW); // turn the LED off by making the voltage LOW
  //delay(10000); // wait for a second
  val = analogRead(analogPin);  // read the input pin
  Serial.println(val);          // debug value
  
//aimain chauffe

}
