/*
   Title: capteur_capacitif.ino
 Authors: Simone Vitale and Ellias Ouberri
 Date: 18/03/2021
 Licence: MIT license.
 Contributors: the code has been issued form the Arduino examples on the official site http://www.arduino.cc/en/Tutorial.
Code on the capactive captor(wma305) 

*/



int analogPin = A0; // here we declare that an analog pin, the 0 , should be called analogPin, a variable
                    
int val = 0;  // variable to store the value read

// function to declare the setup 
void setup() {
  Serial.begin(9600);           //  setup serial: opens serial port, sets data rate to 9600 bps
}

//cycle on what the programme will do
void loop() {
  val = analogRead(analogPin);  // read the input pin
  Serial.println(val);          // debug value and print on the serial which could be accesed in Tools/serialplotter
}
