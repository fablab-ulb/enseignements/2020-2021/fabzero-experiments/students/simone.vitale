/*
    Title: Halleffect.ino

  Author: Simone Vitale
  Date: 20/04/2021
  Licence: MIT license.

  Contributors: the code has been issued form the union of different examples with some debugging on the code.
 -The information for the display TM1637 has been issued from the site "https://www.makerguides.com/tm1637-arduino-tutorial/", from which I have taken the code for activating it and writing on it.
 - The HALL (HOLZER) MAGNETIC SWITCH MODULE VMA313 has been programmed using the user manual:  "https://www.velleman.eu/downloads/29/vma313_a4v01.pdf"  and here "https://www.electronics-lab.com/project/using-hall-effect-sensor-arduino/"
 - The example code for the ELECTROMAGNET MODULE VMA431 has been issued from the following page(user manual):"https://www.velleman.eu/downloads/29/vma431_a4v01.pdf"


*/



// Include the libraries:

#include <Arduino.h>    //basic library for Arduino programming
#include <TM1637Display.h>  //LIbrary which is dedicated to the 4-digit led screen, the module is called TM1637 and is a display. The library has been suggested from this site https://www.makerguides.com/tm1637-arduino-tutorial/

// Declaration of the pin variables

int Electromagnet = 0;    //pin for giving the input to the electromagnet
int hallSensorPin = 2;    //pin to receive the Hall sensor reaction  
int ledPin =  13;    //pin, with annexed resistance in Arduino, which allows to control the LED
int analogPin = A2; // potentiometer wiper (middle terminal) connected to analog pin 2
                    // outside leads to ground and +5V
int val = 0, val2 = 0 ;  // variable to store the value read by the sensor, which will be then printed
int state=0;
int B_critical = 320; // critical value for the magnetic field, going over this value will activate the LED
int electromagnet = 4; // variable which activate the program without the electromagnet if equal to 0 and activate the electromagnet if a number different from 0
int timeofmagnet= 1000;  //Variable which describes the time for which the magnet must be switched on in ms 
// Define the connections pins for the screen:
#define CLK 7
#define DIO 6

       
// Create display object of type TM1637Display, feel free to change the definition of the pins depending on your circuit:
TM1637Display display = TM1637Display(CLK, DIO);


// Create array that turns all segments on:
const uint8_t data[] = {0xff, 0xff, 0xff, 0xff};
// Create array that turns all segments off:
const uint8_t blank[] = {0x00, 0x00, 0x00, 0x00};
// You can set the individual segments per digit to spell words or create other symbols:
const uint8_t done[] = {
  SEG_B | SEG_C | SEG_D | SEG_E | SEG_G,           // d
  SEG_A | SEG_B | SEG_C | SEG_D | SEG_E | SEG_F,   // O
  SEG_C | SEG_E | SEG_G,                           // n
  SEG_A | SEG_D | SEG_E | SEG_F | SEG_G            // E
};



void setup() {

  // Clear the display from previous results:
  display.clear();
  delay(1000);
  //define the role of the pin
  pinMode(ledPin, OUTPUT);      
  pinMode(hallSensorPin, INPUT);
  
  Serial.begin(9600);           //  setup serial for printing the values of the magnetic field
// initialize the digital pin as an output.
  pinMode(Electromagnet, OUTPUT);
}

//The code will first activate the electromagnet for 1 second,5 seconds etc.. depending on the choice of the delay time and the value of B will then measured. The code will then measure the magnetic field without the magnet.
void loop(){
  
  state = digitalRead(hallSensorPin);

  // Set the brightness of the screen:
  display.setBrightness(7);

  
  if(electromagnet !=0) digitalWrite(Electromagnet, HIGH); // turn the Electromagnet on (HIGH is the voltage level)
  
  delay(timeofmagnet); // wait for a second with the Eletromagnet activated
  
  val = analogRead(analogPin);  // read the input pin of the Hall sensor
  Serial.println(val);          // debug value
  //val2=val*5/1023;   //line which allow to obtain the value of the Hall tension

  // cycle on the LED which allows to activate it or not depending on a critical value which can be defined at the start:
  if (val>B_critical) {        
    digitalWrite(ledPin, HIGH);  
  } 
  if (val<B_critical){
    digitalWrite(ledPin, LOW); 
  }

  // Print the measured value of the density of the magnetic field in gauss:
display.showNumberDec(val);

}
