/*
  Title Blink.ino
  author: Simone Vitale
Date: 20/04/2021
  License: Mit license 
  Turns an LED on for one second, then off for one second, repeatedly.
  This example code can be found in http://www.arduino.cc/en/Tutorial/Blink

*/

// the setup function runs once when you press reset or power the board
void setup() {
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(15, OUTPUT);
  //enable to write in the serial
  SerialUSB.begin(0);
}

// the loop function runs over and over again forever
void loop() {
  digitalWrite(15, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(1000);                       // wait for a second
  digitalWrite(15, LOW);    // turn the LED off by making the voltage LOW
  delay(1000);// wait for a second
  SerialUSB.println("The card can compile the code");

}
